var commonViewModel = {
    convertToUpper: function (value) {
        return value == null ? "" : value.toUpperCase();
    },
    validTableResetField: function(elem){
        var form = $("#" + elem);
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);
        success.show();
        error.hide();
    },
    loadDiv: function(elem){
        App.blockUI({
            target: '#' + elem,
            animate: true
        });
    },
    unloadDiv: function (elem) {
        window.setTimeout(function () {
            App.unblockUI('#' + elem);
        }, 500);
    },
    disabledDiv: function (elem) {
        $(elem).addClass("disabledDiv");
    },
    enabledDiv: function (elem) {
        $(elem).removeClass("disabledDiv");
    },
    setActiveMenu: function (parentli) {
        $("#" + parentli).addClass('active open');
        $("#" + parentli + " ul").css("display", "block");
        $("#" + parentli + " a span").addClass("open");
    },
}