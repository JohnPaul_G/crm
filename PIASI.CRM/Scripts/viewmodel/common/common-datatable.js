var datatableviewmodel = {
    fixedHeaderOffset: 0,
    AddSearchColumn: function (elem, idPrefix) {
        if (idPrefix === undefined) {
            idPrefix = "";
        }

        var i = 1;
        $('#' + elem + ' thead tr:eq(1) th').each(function () {
            var title = $('#' + elem + ' thead tr:eq(0) th').eq($(this).index()).text();

            if (title.toUpperCase().search("Date".toUpperCase()) > -1 || title.toUpperCase().search("Created".toUpperCase()) > -1 || title.toUpperCase().search("Modified".toUpperCase()) > -1) {
                $(this).html('<input type="text" class="searchColumn form-control" name="dateRange' + title.replace(" ", "") + '" id="dateRange' + title.replace(" ", "") + '" placeholder="' + title + '"/>');
            }
            else {
                if (title != "" && title != "Action" && title != ".") {
                    $(this).html('<input type="text" class="searchColumn form-control" style="width: 100%;padding: 3px;box-sizing: border-box;" id="' + idPrefix + i++ + '" placeholder="' + title + '" />');
                }
            }
        });
    },
    InitializeEventsForSearchColumn: function (elem, oTable) {
        var i = 1;
        $('#' + elem + ' thead tr:eq(1) th').each(function () {
            var title = $('#' + elem + ' thead tr:eq(0) th').eq($(this).index()).text();

            if (title.toUpperCase().search("Date".toUpperCase()) > -1 || title.toUpperCase().search("Created".toUpperCase()) > -1 || title.toUpperCase().search("Modified".toUpperCase()) > -1) {
                $('input[name="dateRange' + title.replace(" ", "") + '"]').daterangepicker({
                    opens: 'left',
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="dateRange' + title.replace(" ", "") + '"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                    oTable.fnFilter("");
                });

                $('input[name="dateRange' + title.replace(" ", "") + '"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                    oTable.fnFilter("");
                });
            }
        });

        $("#" + elem + " .searchColumn").keyup(function (e) {
            if (e.keyCode == 13) {
                oTable.fnFilter(this.value);
            }
        });
    },
    FixHeader: function () {
        this.fixedHeaderOffset = 0;
        if (App.getViewPort().width < App.getResponsiveBreakpoint('md')) {
            if ($('.page-header').hasClass('page-header-fixed-mobile')) {
                this.fixedHeaderOffset = $('.page-header').outerHeight(true);
            }
        } else if ($('body').hasClass('page-header-menu-fixed')) { // admin 3 fixed header menu mode
            this.fixedHeaderOffset = $('.page-header-menu').outerHeight(true);
        } else if ($('body').hasClass('page-header-top-fixed')) { // admin 3 fixed header top mode
            this.fixedHeaderOffset = $('.page-header-top').outerHeight(true);
        } else if ($('.page-header').hasClass('navbar-fixed-top')) {
            this.fixedHeaderOffset = $('.page-header').outerHeight(true);
        } else if ($('body').hasClass('page-header-fixed')) {
            this.fixedHeaderOffset = 64; // admin 5 fixed height
        }
    },
    DropdownFilterDataCountBesideInfo: function () {
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '5');
    }
}