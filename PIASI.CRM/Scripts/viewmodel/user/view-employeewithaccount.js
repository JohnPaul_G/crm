﻿$(document).ready(function () {
    module.init();
    module.initEventHandlers();
});

var module = {
    $dataTable: function () {
        module.$dataTable.fnFilter("");
    },
    init: function () {
        this.loadTable();
    },
    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        datatableviewmodel.FixHeader();

        module.$dataTable = $("#dataTable").dataTable({
            ajax: {
                "url": baseUrl + "user/getusergridviewdata",
                "type": "POST",
                "data": function (d) {
                    d.employeeid = $("#1").val(),
                        d.name = $("#2").val(),
                        d.position = $("#3").val(),
                        d.department = $("#4").val(),
                        d.active = $("#5").val();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            },
            columns: [
                {
                    "data": "EmployeeId",
                    "orderable": true
                },
                {
                    "data": "Name",
                    "orderable": true
                },
                {
                    "data": "Position",
                    "orderable": true
                },
                {
                    "data": "Department",
                    "orderable": true,
                },
                {
                    "data": "IsActive",
                    "orderable": false,
                    render: function (data, type, full, meta) {
                        var isactiveData = {};
                        if (data == true) {
                            isactiveData.status = "label-success";
                            isactiveData.text = "Active";
                        }
                        else {
                            isactiveData.status = "label-warning";
                            isactiveData.text = "Deactivated";
                        }
                        return "<span class='label label-lg " + isactiveData.status + "'>" + isactiveData.text + "</span > ";
                    }
                },
                {
                    "data": "EmployeeId",
                    "orderable" : false,
                    render: function (data, type, full, meta) {
                        return "<div class='actions'>" +
                            "<div class='btn-group'>" +
                            "<a class='btn green btn-outline btn-circle btn-sm' href='javascript:;' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' aria-expanded='true'>" +
                            "<i class='fa fa-angle-down'></i></a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li id='js-deactivate' data-employee-id=" + data + "><a href='javascript:;'>Deactivate</a></li>" +
                            "<li id='js-reset-pass' data-employee-id=" + data + "><a href='javascript:;'>Reset Password</a></li>" +
                            "</ul>";
                    }
                }
            ],
            dom: "frtlip", // horizontal scrollable datatable
            filter: false,
            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },
            language: {
                "lengthMenu": "_MENU_",
                "processing": ""
            },
            lengthMenu: [
                [5, 10, 15, 20, 30],
                [5, 10, 15, 20, 30] // change per page values here
            ],
            order: [[0, "asc"]],
            orderCellsTop: true,
            pageLength: 10,
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            },
            pagingType: "bootstrap_full_number",
            processing: true,
            responsive: true,
            serverSide: true,
            stateSave: false
        });

        datatableviewmodel.InitializeEventsForSearchColumn("dataTable", module.$dataTable);
        datatableviewmodel.DropdownFilterDataCountBesideInfo();

        // Disable searching for IsActive column
        $("#dataTable thead tr th>input[type='text']#5,#dataTable thead tr th>input[type='text']#6").css('display', 'none');

        $(".searchColumn").attr("placeholder", "");
        
    },

    initEventHandlers: function () {
        //Deactivate
        $("#dataTable").on("click", ".dropdown-menu #js-deactivate",
            function(e) {
                var empId = $(this).attr("data-employee-id");
                var data = JSON.stringify({
                    empID : parseInt(empId)
                });

                swal({
                    title: "Deactivate Account",
                    text: "Are you sure you want to deactivate the account of this employee ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Ok",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                "url": baseUrl + "user/DeactivateUserAccount",
                                type: "POST",
                                data: data,
                                contentType: 'application/json; charset=utf-8',
                                success: function () {
                                    swal({
                                            title: "Success!",
                                            text: "User Account successfully deactivated.",
                                            type: "success",
                                            confirmButtonClass: "btn-info",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                    });

                                    $("#dataTable").DataTable().ajax.reload();
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.status);
                                    console.log(xhr.responseText);
                                    console.log(thrownError);
                                        swal({
                                            title: "Error!",
                                            text: "Something went wrong. Please try again.",
                                            type: "error",
                                            confirmButtonClass: "btn-info",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        });
                                }
                            });

                        }
                    });
            });
        //Reset Password
        $("#dataTable").on("click", ".dropdown-menu #js-reset-pass",
            function(e) {
                var empId = $(this).attr("data-employee-id");
                var data = JSON.stringify({
                    empID: parseInt(empId)
                });

                swal({
                    title: "Request Reset Password",
                    text: "This will send an email to the employee to reset his/her account.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Ok",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                "url": baseUrl + "user/AjaxResetPassword",
                                type: "POST",
                                data: data,
                                contentType: 'application/json; charset=utf-8',
                                beforeSend: function() {
                                    $("#loading").css('display', 'block');
                                    $(".page-sidebar, .page-content-wrapper").css('opacity', '.5');
                                },
                                success: function (response) {
                                    //console.log(response);
                                    $("#loading").css('display', 'none');
                                    $(".page-sidebar, .page-content-wrapper").css('opacity', '1');
                                    
                                    var swalText = {};
                                    if (response == "False") {
                                        swalText.title = "Error!";
                                        swalText.text = "Something went wrong. Please try again.";
                                        swalText.type = "warning";
                                    } else {
                                        swalText.title = "Success!";
                                        swalText.text = "Email successfully sent!";
                                        swalText.type = "success";
                                    }

                                    swal({
                                        title: swalText.title,
                                        text: swalText.text,
                                        type: swalText.type,
                                        confirmButtonClass: "btn-info",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });

                                    $("#dataTable").DataTable().ajax.reload();
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.status);
                                    console.log(xhr.responseText);
                                    console.log(thrownError);
                                    swal({
                                        title: "Error!",
                                        text: "Something went wrong. Please try again.",
                                        type: "error",
                                        confirmButtonClass: "btn-info",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                }
                            });

                        }
                    });
            });
        // Reload datatable on search and filter field cleared
        $(".searchColumn").on("input",
            function(e) {
                if ($(this).val() === "")
                    //$("#dataTable").DataTable().ajax.reload();
                    $("#dataTable").DataTable().page.len(10).draw();
            });
    }
}