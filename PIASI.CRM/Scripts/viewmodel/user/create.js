﻿$(document).ready(function () {
    module.init();
    module.initEventHandlers();
});

var module = {
    $dataTable: function () {
        module.$dataTable.fnFilter("");
    },
    init: function () {
        this.loadTable();
    },
    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        //datatableviewmodel.FixHeader();

        module.$dataTable = $("#dataTable").dataTable({
            ajax: {
                "url": baseUrl + "user/getemployeewithoutaccountgridview",
                "type": "POST",
                "data": function (d) {
                    d.employeeid = $("#1").val(),
                        d.name = $("#2").val(),
                        d.position = $("#3").val(),
                        d.department = $("#4").val()
                        //d.active = $("#5").val();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            },
            columns: [
                {
                    "data": "EmployeeId",
                    "orderable": false,
                    render: function(data, type, full, meta) {
                        return "<label class='mt-checkbox'><input type='checkbox' id='js-empno' data-employeeid = " + data + " /><span></span></label>";
                    }
                },
                {
                    "data": "EmployeeId",
                    "orderable": true
                },
                {
                    "data": "Name",
                    "orderable": true
                },
                {
                    "data": "Position",
                    "orderable": true
                },
                {
                    "data": "Department",
                    "orderable": true,
                }
                //{
                //    "data": "IsActive",
                //    "orderable": false,
                //    render: function (data, type, full, meta) {
                //        var isChecked = data === true ? "checked" : "";
                //        return "<label class='mt-checkbox'><input type='checkbox' " + isChecked + " disabled /><span></span></label>";
                //    }
                //}
            ],
            dom: "frtlip", // horizontal scrollable datatable
            filter: false,
            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },
            language: {
                "lengthMenu": "_MENU_",
                "processing": ""
            },
            lengthMenu: [
                [5, 10, 15, 20, 30],
                [5, 10, 15, 20, 30] // change per page values here
            ],
            order: [[0, "asc"]],
            orderCellsTop: true,
            pageLength: 10,
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            },
            pagingType: "bootstrap_full_number",
            processing: true,
            responsive: true,
            serverSide: true,
            stateSave: false
        });

        datatableviewmodel.InitializeEventsForSearchColumn("dataTable", module.$dataTable);
        datatableviewmodel.DropdownFilterDataCountBesideInfo();

        // Disable searching for IsActive column
        //$("#dataTable thead tr th>input[type='text']#5").css('display', 'none');

        // Clear the label of search boxes
        $(".searchColumn").attr("placeholder", "");
    },
    initEventHandlers: function () {

        // BEGINNING Invite User button
        $("#js-invite-user").on("click",
            function (e) {
                var empIds = $("#dataTable input:checkbox:checked").map(function () {
                    return $(this).attr("data-employeeid");
                }).get();

                var empIdsLength = empIds.length;
                if (empIdsLength > 0) {
                    var title = empIdsLength > 1 ? "these Employees" : "this Employee";
                    var text = empIdsLength > 1 ? "them" : "him/her"
                    swal({
                            title: "Are you sure you want to invite " + title + "?",
                            text: "This will send " + text + " an email to create an account.",
                            type: "info",
                            showCancelButton: true,
                            confirmButtonClass: "btn-info",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                $.ajax({
                                    url: "/api/user/SendAddUserRequestEmail",
                                    type: "POST",
                                    data: JSON.stringify(empIds),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    beforeSend: function() {
                                        $("#loading").css('display', 'block');
                                        $(".page-sidebar, .page-content-wrapper").css('opacity', '.5');
                                    },
                                    success: function (json) {
                                        $("#loading").css('display', 'none');
                                        $(".page-sidebar, .page-content-wrapper").css('opacity', '1');
                                        $("#dataTable").DataTable().ajax.reload();

                                        var successArr = [];
                                        var failArr = [];
                                        var successResult = "";
                                        var failResult = "";

                                        for (var i = 0; i < json.length; i++) {
                                            if (json[i].Result === true) {
                                                successArr.push(json[i]);
                                                successResult += " " + json[i].EmpId + ",";
                                            } else {
                                                failArr.push(json[i]);
                                                failResult += " " + json[i].EmpId + ",";
                                            }
                                        }

                                        successResult = successResult.replace(/,\s*$/, ".");
                                        failResult = failResult.replace(/,\s*$/, ".");

                                        if (successArr.length > 0) {
                                            swal({
                                                title: "Success!",
                                                text: "Email Successfully Sent on the following Employee ID: " + successResult,
                                                type: "success",
                                                confirmButtonClass: "btn-info",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            });
                                        }
                                        if (failArr.length > 0) {
                                            swal({
                                                title: "Fail",
                                                text: "An error occured while sending email to these Employee ID: " + failResult,
                                                type: "warning",
                                                confirmButtonClass: "btn-info",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            });
                                        }
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {
                                        console.log(xhr.status);
                                        console.log(xhr.responseText);
                                        console.log(thrownError);
                                    }
                                });
                            }
                        });
                } else
                    swal({
                        title: "Warning!",
                        text: "Select at least one record to invite.",
                        type: "warning",
                        confirmButtonClass: "btn-info",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
            });// END Invite User button

        // Reload datatable on search and filter field cleared
        $(".searchColumn").on("input",
            function (e) {
                if ($(this).val() === "")
                    //$("#dataTable").DataTable().ajax.reload();
                    $("#dataTable").DataTable().page.len(10).draw();
            });
    }
}