﻿$(document).ready(function () {
    module.initEventHandlers();
    module.initEditorFormValidation();
});

var module = {
    initEventHandlers: function () {
        $("#userFormBtn").on("click",
            function (e) {
                var form = $("#userForm");
                var error = $(".alert-danger", form);
                var success = $(".alert-success", form);
                success.show();
                error.hide();

                var btnLabel = $(this).text();
                var swalText = {};

                if (btnLabel == "Register") {
                    // Swal for account registration
                    swalText.title = "Confirm Registration";
                    swalText.text = "Your account will be created.";
                } else {
                    // Swal for reset password
                    swalText.title = "Confirm reset password";
                    swalText.text = "Your password will be reset";
                }

                swal({
                    title: swalText.title,
                    text: swalText.text,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm)
                        form.submit();
                });

            });
    },
    initEditorFormValidation: function() {
        var form = $("#userForm");
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);

        form.validate({
            errorElement: "span", //default input error message container
            errorClass: "help-block help-block-error", //default error class
            focusInvalid: false, //do not focus the last invalid input
            ignore: "", //validae all fields including form hidden input
            rules: {
                Email: {
                    required: true,
                    email: true
                },
                Password: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent(".input-icon").children("i");
                icon.removeClass("fa-check").addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ "container": "body" });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest(".input-group").removeClass("has-success")
                    .addClass("has-error"); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
            },

            success: function (label, element) {
                var icon = $(element).parent(".input-icon").children("i");
                $(element).closest(".input-group").removeClass("has-error")
                    .addClass("has-success"); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            }
        });
    },
    initFormValidations: function () {

        jQuery.validator.addMethod("email", function (value, element) {
            // allow any non-whitespace characters as the host part
            return this.optional(element) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test(value);
        }, 'Please enter a valid email address.');
        
        this.initEditorFormValidation();
    }
};