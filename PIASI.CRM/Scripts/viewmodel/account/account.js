﻿$(document).ready(function () {
    module.initEventHandlers();
});

var module = {
    initEventHandlers: function () {
        $("#isPasswordValid").on("click", function (e) {
            var password = $("#txtCurrentPass").val();
            $.ajax({
                url: "/Account/CheckPasswordValidity?password=" + password,
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == "True") {
                        $("#isPasswordValid").css('display', 'none');
                        $("#txtCurrentPass").after("<span id='response' style='padding-left: 5px; color: green;'><i class='fa fa-check'> Password correct.</i></span>");
                        $("#account-validated").fadeIn();
                        setTimeout(function () {
                            //$("#response").fadeOut();
                            $("#response").css('display', 'none');
                            $("#txtCurrentPass").css('flex', '7').attr('readonly', 'true');
                        },1500)
                    }
                    else
                    {
                        $("#isPasswordValid").after("<span id='response' style='padding-left: 5px; color: red;'><i class='fa fa-times-circle'> Incorrect Password.</i></span>");
                        $(".portlet-body").css('width', '85%');
                        setTimeout(function () {
                            //$("#response").fadeOut();
                            $("#response").css('display', 'none');
                            $(".portlet-body").css('width', '80%');
                        }, 1500);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        });
    }
}