﻿$(document).ready(function () {
    module.init();

});

var module = {
    leadId: null,
    leadStatusId: null,
    leadSourceId: null,
    industryId: null,
    createdBy: null,
    createdAt: null,
    updatedBy: null,
    updatedAt: null,

    init: function () {
        module.loadAssignedUserList();
        module.loadLeadStatus();
        module.loadLeadSource();
        module.loadIndustry();
        module.loadCreatedBy();
        module.loadUpdatedBy();

    },


    loadAssignedUserList: function () {
        // 1st GET ALL List with account id
        $.getJSON("/api/lead/getallleadassigneduserbyleadid/" + module.leadId + "", function (data) {
            //var control = $("#leadAssignedUserList");
            //$.each(data, function (index, value) {
            //    if (value.LeadId > 0) {
            //        control.append("<option value='" + value.UserAccountId + "' id ='" + value.EmployeeId + "' selected='selected'>" + value.EmployeeName + ' - ' + value.DepartmentName + "</option>");
            //    }
            //    else {
            //        if ($("#leadAssignedUserList option[id='" + value.EmployeeId + "']").length <= 0) {
            //            control.append("<option value='" + value.UserAccountId + "' id ='" + module.EmployeeId + "'>" + value.EmployeeName + ' - ' + value.DepartmentName + "</option>");
            //        }
            //    }
            //});


            var control = $("#assignedUsers");
            if (data.length <= 0) {
                control.append('<div class="label font-blue-soft bold"> NONE </div>');
            } else {
                $.each(data, function (index, value) {
                    control.append('<div class="label font-blue-soft bold">' +
                        value.EmployeeName + ' - ' + value.DepartmentName +
                                   '</div><br>');
                });
            }
        });
    },

    loadLeadStatus: function () {
        $.getJSON("/api/leadstatus/getleadstatusbyid/" + module.leadStatusId, function (dataStatus) {
            if (dataStatus === null) {
                $("#leadStatus").text("NONE");
            } else {
                $("#leadStatus").text(dataStatus.Name);
            }
        });
    },

    loadLeadSource: function () {
        $.getJSON("/api/leadsource/getleadsourcebyid/" + module.leadSourceId, function (dataSource) {
            if (dataSource === null) {
                $("#leadSource").text("NONE");
            } else {
                $("#leadSource").text(dataSource.Name);
            }
        });
    },

    loadIndustry: function () {
        $.getJSON("/api/industry/getindustrybyid/" + module.industryId, function (dataIndustry) {
            if (dataIndustry === null) {
                $("#industry").text("NONE");
            } else {
                $("#industry").text(dataIndustry.Name);
            }
        });
    },

    loadCreatedBy: function () {
        $.getJSON("/api/lead/getusernamebyuserid/" + module.createdBy, function (data) {
            $("#createdBy").text(data.UserAccount.Employee.Name);
            $("#createdAt").text($.timeago(module.createdAt));
        });
    },

    loadUpdatedBy: function () {
        $.getJSON("/api/lead/getusernamebyuserid/" + module.updatedBy, function (data) {
            $("#updatedBy").text(data.UserAccount.Employee.Name);
            $("#updatedAt").text($.timeago(module.updatedAt));
        });

    }
};