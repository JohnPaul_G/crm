﻿//$(document).ajaxComplete(function (event, xhr, settings) {
//    console.log("settings.url: " + settings.url);

//    if (settings.url === "ajax/test.html") {
//        $(".log").text("Triggered ajaxComplete handler. The result is " +
//          xhr.responseText);
//    }
//});

$(document).ready(function () {
    module.init();
    module.initEventHandlers();
    module.initFormValidations();

});

var module = {
    isEdit: false,
    originalValues: {},
    leadId: null,
    result: "",
    heading: "",
    leadStatusIdSelected: null,
    leadSourceIdSelected: null,
    leadIndustrySelected: null,

    formIsDirty: function () {
        if (!this.isEdit) { // Add
            if ($("#LastName").val() !== "") {
                return true;
            }
            if ($("#FirstName").val() !== "") {
                return true;
            }
            if ($("#MiddleName").val() !== "") {
                return true;
            }
            if ($("#leadPhoneList").val() !== "") {
                return true;
            }
            if ($("#leadEmailList").val() !== "") {
                return true;
            }
            if ($("#Country").val() !== "") {
                return true;
            }
            //if ($("#leadStatusList").val() !== null) {
            //    return true;
            //}
            //if ($("#leadSourceList").val() !== null) {
            //    return true;
            //}
            //if ($("#industryList").val() !== null) {
            //    return true;
            //}

            return false;
        }
        else { // Edit           
            if ($("#LastName").val() !== this.originalValues.lastName) {
                return true;
            }
            if ($("#FirstName").val() !== this.originalValues.firstName) {
                return true;
            }
            if ($("#MiddleName").val() !== this.originalValues.middleName) {
                return true;
            }
            if ($("#leadPhoneList").val() !== this.originalValues.leadPhoneList) {
                return true;
            }
            if ($("#leadEmailList").val() !== this.originalValues.leadEmailList) {
                return true;
            }
            //if ($("#leadStatusList").val() !== this.originalValues.leadStatusId) {
            //    return true;
            //}
            //if ($("#leadSourceList").val() !== this.originalValues.leadSourceId) {
            //    return true;
            //}
            //if ($("#industryList").val() !== this.originalValues.industryId) {
            //    return true;
            //}

            return false;
        }
    },

    init: function () {
        if (module.result === "OK") {
            //swal("Industry sucessfully saved.", "", "success");
            window.location.href = "/lead";
        }
      
        $("#div-help-textPhone").display = "block";
        $("#div-help-textEmail").display = "block";

        commonViewModel.setActiveMenu("lead");

        $("#LeadStatusId").select2({
            placeholder: "SELECT ONE"
        });
        $("#LeadSourceId").select2({
            placeholder: "SELECT ONE"
        });
        $("#IndustryId").select2({
            placeholder: "SELECT ONE"
        });
        $("#leadAssignedUserList").select2({
            placeholder: "SELECT ONE"
        });
        $("#leadPhoneList").select2({
            
        });
        $("#leadEmailList").select2({
            //tags: true,
            //tokenSeparators: [',', ' '],

            //insertTag: function (data, tag) {
            //    // Insert the tag at the end of the results
            //    data.push(tag);
            //}
        });

        module.loadAssignedUserList();
        module.loadLeadStatusList();
        module.loadLeadSourceList();
        module.loadIndustryList();

        if (this.isEdit) { // Edit Mode
            this.originalValues.lastName = $("#LastName").val();
            this.originalValues.firstName = $("#FirstName").val();
            this.originalValues.middleName = $("#MiddleName").val();
            this.originalValues.leadPhoneList = $("#leadPhoneList").val();
            this.originalValues.leadEmailList = $("#leadEmailList").val();
            //this.originalValues.leadStatusId = $("#leadStatusList").val();
            //this.originalValues.leadSourceId = $("#leadSourceList").val();
            //this.originalValues.industryId = $("#industryList").val();
        }
        $("#Name").focus();
    },

    initEditorFormValidation: function () {
        var form = $("#editorForm");
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);

        form.validate({
            errorElement: "span", //default input error message container
            errorClass: "help-block help-block-error", // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                LastName: {
                    required: true
                },
                FirstName: {
                    required: true
                },
                MiddleName: {
                    required: true
                },
                leadPhoneList: {
                    required: true
                },
                leadEmailList: {
                    required: true
                }
                //leadStatusList: {
                //    required: true
                //},+
                //leadSourceList: {
                //    required: true
                //},
                //industryList: {
                //    required: true
                //}

            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent(".input-icon").children("i");
                icon.removeClass("fa-check").addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ "container": "body" });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest(".form-input-group").removeClass("has-success").addClass("has-error"); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
            },

            success: function (label, element) {
                var icon = $(element).parent(".input-icon").children("i");
                $(element).closest(".form-input-group").removeClass("has-error").addClass("has-success"); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            }
        });
    },

    initEventHandlers: function () {
        // Create Button
        $("#createBtn").click(function () {
            if ($("#editorForm").valid()) {
                var form = $("#editorForm");
                var error = $(".alert-danger", form);
                var success = $(".alert-success", form);
                success.show();
                error.hide();

                swal({
                    title: "Are you sure?",
                    text: "This will save the new lead.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // Submit the Form
                        $("#editorForm").submit();
                    }
                    else { }
                });
            }
        });

        // Save Button
        $("#saveBtn").click(function () {
            if ($("#editorForm").valid()) {
                var form = $("#editorLogForm");
                var error = $(".alert-danger", form);
                var success = $(".alert-success", form);
                success.show();
                error.hide();

                swal({
                    title: "Are you sure?",
                    text: "This will update the lead.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // Submit the Form
                        $("#editorForm").submit();
                    }
                    else { }
                });
            }
        });

        // Cancel Button
        $("#cancelBtn").click(function () {
            if (module.formIsDirty()) {
                swal({
                    title: "Are you sure you want to cancel?",
                    text: "Any unsaved changes will be lost.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "/lead";
                    }
                    else { }
                });
            }
            else {
                window.location.href = "/lead";
            }
        });

        //numeric only
        $("#textPhone").on('keydown', function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                $("#textPhone").closest(".form-input-group").removeClass("has-success")
                    .addClass("has-error"); // set error class to the control group 
                $("#help-textPhone").closest(".form-input-group").removeClass("has-success").addClass("has-error");
                $("#help-textPhone").text("Please enter numbers only");
                $("#div-help-textPhone").display = "block";
                e.preventDefault();
            } else {
                $("#textPhone").closest(".form-input-group").removeClass("has-success");
                $("#textPhone").closest(".form-input-group").removeClass("has-error");
                $("#help-textPhone").text("");
                $("#div-help-textPhone").display = "none";
            }
        });

        $("#textPhone").on('focusout', function (e) {
            $("#textPhone").val("");

            $("#textPhone").closest(".form-input-group").removeClass("has-success");
            $("#textPhone").closest(".form-input-group").removeClass("has-error");
            $("#help-textPhone").text("");
            $("#div-help-textPhone").display = "none";
        });

        $("#textPhone").on('keyup', function (e) {
            if (e.keyCode === 13) { // press enter
                var drdown = $("#selectPhone").val();
                var txt = $("#textPhone").val();

                // check if existing
                var icount = 0;
                $("#leadPhoneList option").each(function () {
                    if ($(this).text() === drdown + "-" + $.trim(txt)) {
                        icount = icount + 1;
                    }
                });
                var phoneLenght = $("#textPhone").val().length;
                var errorMsg = "";
                if (phoneLenght < 6) {
                    errorMsg = "Please enter atleast 6 characters"; 
                } else if (icount > 11) {
                    errorMsg = "Maximum of 11 characters allowed";
                } else if (icount > 0) {
                    errorMsg = "Phone is already exist";
                } else if ($.isNumeric(txt) === false) {
                    errorMsg = "Please enter numbers only";
                }
                
                if (errorMsg === "") { // success
                    $("#leadPhoneList").append("<option id='" + drdown + "-" + txt + "' value='0-" + drdown + "-" + txt + "' selected='selected'>" + drdown + "-" + txt + "</option>");
                    $("#textPhone").val("");

                    $("#textPhone").closest(".form-input-group").removeClass("has-success");
                    $("#textPhone").closest(".form-input-group").removeClass("has-error");
                    $("#help-textPhone").text("");
                    $("#div-help-textPhone").display = "none";

                    // enabling checkbox
                    $("#CallAvailability").prop('disabled', false); 
                    $("#DoNotCall").prop('disabled', false); 
                } else { // add validation error messages
                    $("#textPhone").closest(".form-input-group").removeClass("has-success").addClass("has-error"); // set error class to the control group 
                    $("#help-textPhone").closest(".form-input-group").removeClass("has-success").addClass("has-error");
                    $("#div-help-textPhone").display = "block";
                    $("#help-textPhone").text(errorMsg);
                }
            }
        });

        $("#textEmail").on('keyup', function (e) {
            if (e.keyCode === 13) {
                var txt = $("#textEmail").val();

                // check if existing
                var icount = 0;
                $("#leadEmailList option").each(function () {
                    if ($(this).text() === $.trim(txt)) {
                        icount = icount + 1;
                    }
                });

                var errorMsg = "";
                if (icount > 0) {
                    errorMsg = "Email address is already exist";
                } else if (!module.validateEmail(txt)) {
                    errorMsg = "Email address is not valid";
                } else if ($.trim(txt) === "") {
                    errorMsg = "Please input email address";
                }

                if (errorMsg === "") { //success //(icount === 0 && $.trim(txt) !== "") {
                    $("#leadEmailList").append("<option id='" +
                        txt +
                        "' value='0-" +
                        txt +
                        "' selected='selected'>" +
                        txt +
                        "</option>");
                    $("#textEmail").val("");

                    $("#textEmail").closest(".form-input-group").removeClass("has-success");
                    $("#textEmail").closest(".form-input-group").removeClass("has-error");
                    $("#help-textEmail").text("");
                    $("#div-help-textEmail").display = "none";
                } else { // add validation error messages
                    $("#textEmail").closest(".form-input-group").removeClass("has-success").addClass("has-error"); // set error class to the control group 
                    $("#help-textEmail").closest(".form-input-group").removeClass("has-success").addClass("has-error");
                    $("#div-help-textEmail").display = "block";
                    $("#help-textEmail").text(errorMsg);
                }
            }
        });

        $("#textEmail").on('focusout', function (e) {
            $("#textEmail").val("");
  
            $("#textEmail").closest(".form-input-group").removeClass("has-success");
            $("#textEmail").closest(".form-input-group").removeClass("has-error");
            $("#help-textEmail").text("");
            $("#div-help-textEmail").display = "none";
        });

        $("#leadPhoneList").on('change.select2', function () {
            var isDisabled = false;
            if ($(this).select2('data').length <= 0) {
                isDisabled = true;
                $("#CallAvailability").val(""); // clear textbox
                $("#DoNotCall").prop('checked', false); // uncheck checkbox
            }
            $("#CallAvailability").prop('disabled', isDisabled); // enabled/disabled textbox
            $("#DoNotCall").prop('disabled', isDisabled); // enabled/disabled checkbox
        });

        //delete option when deselect
        $("#leadEmailList").on('select2:unselect', function(e) {
            var data = e.params.data;
            $("[id='" + data.text + "']").remove();
        });

        $("#leadPhoneList").on('select2:unselect', function (e) {
            var data = e.params.data;
            $("[value='" + data.id + "']").remove();
        });
    },

    initFormValidations: function () {
        // Alpha-Numeric
        jQuery.validator.addMethod("alphanumeric", function (value, element) {
            // Alpha-Numeric and Underscore
            //return this.optional(element) || /^[\w.]+$/i.test(value);

            // Alpha-Numeric
            return this.optional(element) || /^[a-z0-9]+$/i.test(value);

        }, "Letters and numbers only please.");

        this.initEditorFormValidation();
    },

    loadAssignedUserList: function () {
        // 1st GET ALL List with account id
        $.getJSON("/api/lead/getleadassigneduserlistbyleadid/" + module.leadId + "", function (data) {
            var control = $("#leadAssignedUserList");
            $.each(data, function (index, value) {
                if (value.LeadId > 0) {
                    control.append("<option value='" + value.UserAccountId + "' id ='" + value.EmployeeId + "' selected='selected'>" + value.EmployeeName + ' - ' + value.DepartmentName + "</option>");
                }
                else {
                    if ($("#leadAssignedUserList option[id='" + value.EmployeeId + "']").length <= 0) {
                        control.append("<option value='" + value.UserAccountId + "' id ='" + module.EmployeeId + "'>" + value.EmployeeName + ' - ' + value.DepartmentName + "</option>");
                    }
                }
            });
        });
    },

    validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    },

    loadLeadStatusList: function () {
        $.getJSON("/api/leadstatus/getleadstatuslist", function (data) {
            var control = $("#LeadStatusId");
            control.append("<option value=0> </option>");
            $.each(data, function (index, value) {
                control.append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });

            // Set the Selected Option (Edit/View Mode)
            var id = module.leadStatusIdSelected;
            if (id !== "") {
                $("#LeadStatusId").val(id);
                module.originalValues.leadStatusId = id;
            }
        });
    },

    loadLeadSourceList: function () {
        $.getJSON("/api/leadsource/getleadsourcelist", function (data) {
            var control = $("#LeadSourceId");
            control.append("<option value=0> </option>");
            $.each(data, function (index, value) {
                control.append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });

            // Set the Selected Option (Edit/View Mode)
            var id = module.leadSourceIdSelected;
            if (id !== "") {
                $("#LeadSourceId").val(id);
                module.originalValues.leadSourceId = id;
            }
        });
    },

    loadIndustryList: function () {
        $.getJSON("/api/industry/getindustrylist", function (data) {
            var control = $("#IndustryId");
            control.append("<option value=0> </option>");
            $.each(data, function (index, value) {
                control.append("<option value='" + value.Id + "'>" + value.Name + "</option>");
            });

            // Set the Selected Option (Edit/View Mode)
            var id = module.leadIndustrySelected;
            if (id !== "") {
                $("#IndustryId").val(id);
                module.originalValues.industryId = id;
            }
        });
    }
};