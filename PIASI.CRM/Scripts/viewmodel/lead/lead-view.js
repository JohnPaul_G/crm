﻿$(document).ready(function () {
    module.init();
    module.initEventHandlers();
});


var module = {
    $dataTable: function () {
        module.$dataTable.fnFilter("");
    },

    init: function () {
        commonViewModel.setActiveMenu("lead");
        this.loadTable();
        $(".searchColumn").removeAttr("placeholder");
    },

    initEventHandlers: function () {
        $("#dataTable").on("click", ".removeBtn", function () {
            // Prepare Values
            var id = $(this).data("id");

            swal({
                title: "Are you sure?",
                text: "This will remove the lead from the list.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "api/lead/remove?" + $.param({ "id": id }),
                        type: "DELETE",
                        success: function (result) {
                            if (result === "OK") {
                                swal("Lead sucessfully deleted.", "", "success");
                                module.$dataTable.fnFilter("");
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
                else { }
            });
        });
    },

    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        datatableviewmodel.FixHeader();

        module.$dataTable = $("#dataTable").dataTable({
            ajax: {
                "url": baseUrl + "lead/getgridviewdata",
                "type": "POST",
                "data": function (d) {
                    d.name = $("#1").val(),
                    d.company = $("#2").val(),
                    d.email = $("#3").val(),
                    d.website = $("#4").val(),
                    d.assignedUser = $("#5").val(),
                    d.createdAt = $("#dateRangeCreatedAt").val();
                }
            },
           
            columns: [
               {
                   "data": "Name",
                   "orderable": true
               },
               {
                   "data": "Company",
                   "orderable": true
               },
               {
                   "data": "Email",
                   "orderable": true
               },
               {
                   "data": "Website",
                   "orderable": true
               },
               {
                   "data": "AssignedUser",
                   "orderable": true
               },
               {
                   "data": "CreatedAt",
                   "orderable": true
               },
               {
                   "data": "Id",
                   "orderable": false,
                   "render": function (data, type, full, meta) {
                       //return "<a href='/lead/view/" + data + "' class='btn btn-xs btn-circle blue btn-outline' title='View'>View</a> <a href='/lead/edit/" + data + "' class='btn btn-xs btn-circle blue btn-outline editBtn' title='Edit'>Edit</a><button type='button' class='btn btn-xs btn-circle red btn-outline removeBtn' data-id='" + data + "' title='Remove'>Delete</button>";

                       return "<div class='actions'>" +
                            "<div class='btn-group'>" +
                            "<a class='btn green btn-outline btn-circle btn-sm' href='javascript:;' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' aria-expanded='true'>" +
                            "<i class='fa fa-angle-down'></i></a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li><a href='/lead/view/" + data + "'>View</a></li>" +
                            "<li><a href='/lead/edit/" + data + "'>Edit</a></li>" +
                            "<li><a type='button' class='removeBtn' data-id='" + data + "' title='Remove'>Delete</a></li>" +
                            // "<li><a type='button' class='btn removeBtn' data-id='" + data + "' title='Remove'>Delete</a></li>" +
                            "</ul>";
                   }
               }
            ],
            dom: "frtlip", // horizontal scrollable datatable
            filter: false,
            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },
            language: {
                "lengthMenu": "_MENU_",
                "processing": ""
            },
            lengthMenu: [
                [5, 10, 15, 20, 30],
                [5, 10, 15, 20, 30] // change per page values here
            ],
            order: [[0, "asc"]],
            orderCellsTop: true,
            pageLength: 10,
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            },
            pagingType: "bootstrap_full_number",
            processing: true,
            responsive: true,
            serverSide: true,
            stateSave: false
        });

        datatableviewmodel.InitializeEventsForSearchColumn("dataTable", module.$dataTable);
        datatableviewmodel.DropdownFilterDataCountBesideInfo();

        // Reload datatable on search and filter field cleared
        $(".searchColumn").on("input", function (e) {
            if ($(this).val() === "") {
                $("#1").val("");
                $("#2").val("");
                $("#3").val("");
                $("#4").val("");
                $("#5").val("");
                $("#dateRangeCreatedAt");
                $("#dataTable").DataTable().page.len(10).draw();
            }      
        });
    }
}