﻿$(document).ready(function() {
    module.init();
    module.initEventHandlers();
    module.initFormValidations();
});

var module =
{
    isEdit: false,
    values: {},
    employeeid: null,
    result: "",

    formIsDirty: function() {
        if (!this.isEdit) {
            if ($("#EmployeeId").val() !== "") {
                return true;
            }
            if ($("#LastName").val() !== "") {
                return true;
            }
            if ($("#FirstName").val() !== "") {
                return true;
            }
            if ($("#MiddleName").val() !== "") {
                return true;
            }
            if ($("#Email").val() !== "") {
                return true;
            }
            if ($("#ContactNumber").val() !== "") {
                return true;
            }
            if ($("#Birthdate").val() !== "") {
                return true;
            }
            if ($("#Position").val() !== "") {
                return true;
            }
            if ($("#Department").val() !== "") {
                return true;
            }

            return false;
        }
        else {
            if ($("#EmployeeId").val() !== this.values.employeeid) {
                return true;
            }
            if ($("#LastName").val() !== this.values.lastname) {
                return true;
            }
            if ($("#FirstName").val() !==this.values.firstname) {
                return true;
            }
            if ($("#MiddleName").val() !== this.values.middlename) {
                return true;
            }
            if ($("#Email").val() !== this.values.email) {
                return true;
            }
            if ($("#ContactNumber").val() !== this.values.contactnumber) {
                return true;
            }
            if ($("#Birthdate").val() !== this.values.birthdate) {
                return true;
            }
            if ($("#Position").val() !== this.values.position) {
                return true;
            }
            if ($("#Department").val() !== this.values.department) {
                return true;
            }

            return false;
        }
    },

    init: function() {
        if (module.result === "OK") {
            swal("Employee sucessfully saved.", "", "success");
            window.location.href = "/Employee";
        }

        if (this.isEdit) {
            this.values.employeeid = $("#EmployeeId").val();
            this.values.lastname = $("#LastName").val();
            this.values.firstname = $("#FirstName").val();
            this.values.middlename = $("#MiddleName").val();
            this.values.email = $("#Email").val();
            this.values.contactnumber = $("#ContactNumber").val();
            this.values.birthdate = $("#Birthdate").val();
            this.values.position = $("#Position").val();
            this.values.department = $("#Department").val();
        }

        $("#EmployeeId").focus();
    },

    initEditorFormValidation: function() {
        var form = $("#employeeForm");
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);

        form.validate({
            errorElement: "span", //default input error message container
            errorClass: "help-block help-block-error", //default error class
            focusInvalid: false, //do not focus the last invalid input
            ignore: "", //validae all fields including form hidden input
            rules: {
                EmployeeId: {
                    numbers: true,
                    required:true,
                    existing:true
                },
                LastName: {
                    required: true
                },
                FirstName: {
                    required: true
                },
                MiddleName: {
                    required: true
                },
                Email: {
                    required: true,
                    email:true
                },
                ContactNumber: {
                    numbers: true,
                    required: true
                },
                Birthdate: {
                    required: true
                },
                Position: {
                    required: true
                },
                Department: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
            },

            errorPlacement: function(error, element) { // render error placement for each input type
                var icon = $(element).parent(".input-icon").children("i");
                icon.removeClass("fa-check").addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ "container": "body" });
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest(".form-input-group").removeClass("has-success")
                    .addClass("has-error"); // set error class to the control group   
            },

            unhighlight: function(element) { // revert the change done by hightlight
            },

            success: function(label, element) {
                var icon = $(element).parent(".input-icon").children("i");
                $(element).closest(".form-input-group").removeClass("has-error")
                    .addClass("has-success"); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            }
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
        })
    },

    initEventHandlers : function(){
//Add Button
    $("#Add").click(function() {
    if ($("#employeeForm").valid()) {
        var form = $("#employeeForm");
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);
        success.show();
        error.hide();

        swal({
                title: "Are you sure?",
                text: "This will save a new employee.",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    //Submit form
                    $("#employeeForm").submit();
                }
            });
    }
});

//Edit Button
$("#Edit").
    click(function() {
        if ($("#employeeForm").valid()) {
            var form = $("#employeeForm");
            var error = $(".alert-danger", form);
            var success = $(".alert-success", form);
            success.show();
            error.hide();

            swal({
                    title: "Are you sure?",
                    text: "This will update the employee.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        //Submit form
                        $("#employeeForm").submit();
                    }
                });
        }
    });

//Cancel Button
$("#cancelBtn").click(function() {
    if (module.formIsDirty()) {
        swal({
                title: "Are you sure you want to cancel?",
                text: "Any unsaved changes will be lost.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "/Employee";
                }
                else {
                }
            });
    } else {
        window.location.href = "/Employee"
    }
});

//Home Link
$("a#home").click(function () {
    if (module.formIsDirty()) {
        swal({
            title: "Are you sure you want to cancel?",
            text: "Any unsaved changes will be lost.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = "/";
                }
                else {
                }
            });
    } else {
        window.location.href = "/"
    }
});

//Employees Link
$("a#employees").click(function () {
    if (module.formIsDirty()) {
        swal({
            title: "Are you sure you want to cancel?",
            text: "Any unsaved changes will be lost.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = "/Employee";
                }
                else {
                }
            });
    } else {
        window.location.href = "/Employee"
    }
});

},

initFormValidations:function() {
    jQuery.validator.addMethod("numbers",
        function(value, element) {
            //Numbers
            return this.optional(element) || /^[0-9]+$/i.test(value);
        },
        "Numbers only please.");

    //Existing Employee Number
    jQuery.validator.addMethod("existing",
        function(value, element) {
            let result = "";
            let isValid = false;
            $.ajax({
                url: "/api/employee/idexists/" + value ,
                async: false,
                success: function(data) {
                    result = "SUCCESS";
                    isValid = data;
                },
                error: function() {
                    result = "ERROR";
                    isValid = false;
                }
            });

            while (result === "") {
            }
            return isValid;
        },
        "Employee ID is already existing.");

    this.initEditorFormValidation();
}

};