﻿$(document).ready(function () {
    module.init();
    module.initEventHandlers();
});


var module = {
    $dataTable: function () {
        module.$dataTable.fnFilter("");
    },

    init: function () {
        commonViewModel.setActiveMenu("Employee Maintenance");
        this.loadTable();
        $(".searchColumn").removeAttr("placeholder");
    },

    initEventHandlers: function () {
        $("#dataTable").on("click", ".dropdown-menu #deleteEmp", function () {
            // Prepare Values
            let id = $(this).attr("data-id");

            swal({
                title: "Are you sure?",
                text: "This will remove the employee from the list.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "api/employee/remove?" + $.param({ "id": id }),
                        type: "DELETE",
                        success: function (result) {
                            if (result === "OK") {
                                swal("Employee sucessfully deleted.", "", "success");
                                module.$dataTable.fnFilter("");
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
                else { }
            });
        });

        $(".searchColumn").on("input",
            function (e) {
                if ($(this).val() === "")
                {
                    module.$dataTable.fnFilter("");
                }
            });
    },

    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        datatableviewmodel.FixHeader();

        module.$dataTable = $("#dataTable").dataTable({
            ajax: {
                "url": baseUrl + "Employee/GetEmployeeDataTableData",
                "type": "POST",
                "data": function (d) {
                    d.employeeid = $("#1").val(),
                        d.name = $("#2").val(),
                        d.email = $("#3").val(),
                        d.birthdate = $("#dateRangeBirthdate").val(),
                        d.contactnumber = $("#4").val(),
                        d.position = $("#5").val(),
                        d.department = $("#6").val();
                }
            },
            columns: [
                {
                    "data": "EmployeeId",
                    "orderable": true
                },
                {
                    "data": "Name",
                    "orderable": true,
                },
                {
                    "data": "Email",
                    "orderable": true
                },
                {
                    "data": "Birthdate",
                    "orderable": true
                },
                {
                    "data": "ContactNumber",
                    "orderable": true
                },
                {
                    "data": "Position",
                    "orderable": true
                },
                {
                    "data": "Department",
                    "orderable": true
                },
                {
                    "data": "Id",
                    "orderable": false,
                    "render": function (data) {
                         return "<div class='actions'>" +
                            "<div class='btn-group'>" +
                            "<a class='btn green btn-outline btn-circle btn-sm' href='javascript:;' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' aria-expanded='true'>" +
                            "<i class='fa fa-angle-down'></i></a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li><a href='/Employee/Edit/" +data + "'>Edit</a></li>" +
                            "<li id='deleteEmp' data-id=" +data + "><a href='javascript:;'>Delete</a></li>" +
                            "</ul>";
                    }
                },
            ],
            dom: "frtlip", // horizontal scrollable datatable
            filter: false,
            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },
            language: {
                "lengthMenu": "_MENU_",
                "processing": ""
            },
            lengthMenu: [
                [5, 10, 15, 20, 30],
                [5, 10, 15, 20, 30] // change per page values here
            ],
            order: [[0, "asc"]],
            orderCellsTop: true,
            pageLength: 10,
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            },
            pagingType: "bootstrap_full_number",
            processing: true,
            responsive: true,
            serverSide: true,
            stateSave: false
        });

        datatableviewmodel.InitializeEventsForSearchColumn("dataTable", module.$dataTable);
        datatableviewmodel.DropdownFilterDataCountBesideInfo();
    }
}