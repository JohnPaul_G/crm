$(document).ready(function () {
    commonViewModel.setActiveMenu("sample");
    table1.initialize();
});

var table1 = {
    initialize: function () {
        this.loadTable();
        this.bindEvents();
    },
    bindEvents: function () {
        $(".searchColumn").keyup(function (e) {
            if (e.keyCode == 13) {
                table1.filtertable.fnFilter(this.value);
            }
        });

        // Open Child Grid
        $("#dataTable").on("click", "td.details-control", function () {
            var nTr = $(this).parents('tr')[0];
            var tr = $(this).closest("tr");
            var data = table1.filtertable.fnGetData(tr);

            if (table1.filtertable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                table1.filtertable.fnClose(nTr);
                tr.removeClass("shown");
            }
            else {
                /* Open this row */
                table1.filtertable.fnOpen(nTr, table1.generateChildGrid(data), "");
                tr.addClass("shown");
            }
        });
    },
    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        datatableviewmodel.FixHeader();

        table1.filtertable = $('#dataTable').dataTable({
            filter: false,
            orderCellsTop: true,
            serverSide: true,
            stateSave: false,
            responsive: true,
            language: {
                "lengthMenu": "_MENU_",
            },
            ajax: {
                "url": baseUrl + "Sample/AjaxGetJsonData",
                "type": "POST",
                "data": function (d) {
                    d.code = $("#1").val(),
                        d.firstName = $("#2").val(),
                        d.lastName = $("#3").val(),
                        d.position = $("#4").val();
                }
            },
            columns: [
               {
                   "className": "details-control",
                   "orderable": false,
                   "data": null,
                   "defaultContent": "",
                   "width": "3px"
               },
               { "data": "Id", "visible": false },
               { "data": "Code", "orderable": true },
               { "data": "FirstName", "orderable": true },
               { "data": "LastName", "orderable": true },
               { "data": "Position", "orderable": true },
            ],

            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },

            order: [[1, "desc"]],

            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],

            pageLength: 10,

            dom: "frtlip", // horizobtal scrollable datatable
        });

        datatableviewmodel.DropdownFilterDataCountBesideInfo();
    },
    generateChildGrid: function (d) {
        return "<span style='display:block;text-align:center' width='100%'>No detail associated with request!</span>";
    },
}