﻿$(document).ready(function () {
    module.init();
    module.initEventHandlers();

    //$(".searchColumn").on("input",
    //    function() {
    //        var sCode = $("#1").val();
    //        var sStatus = $("#2").val();

    //        var table = $("#dataTable").dataTable();
           
    //        console.log("gago ka ba?");
    //        if (sCode === "" && sStatus === "") {
    //            module.loadTable();
    //        }
    //    });

});


var module = {
    $dataTable: function () {
        module.$dataTable.fnFilter("");
    },

    init: function () {
        commonViewModel.setActiveMenu("leadstatus");
        this.loadTable();
        $(".searchColumn").removeAttr("placeholder");
    },

    initEventHandlers: function () {
        $("#dataTable").on("click", ".removeBtn", function () {
            // Prepare Values
            var id = $(this).data("id");

            swal({
                title: "Are you sure?",
                text: "This will remove the lead status from the list.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "api/leadstatus/remove?" + $.param({ "id": id }),
                        type: "DELETE",
                        success: function (result) {
                            if (result === "OK") {
                                swal("Lead status sucessfully deleted.", "", "success");
                                module.$dataTable.fnFilter("");
                            }
                        },
                        error: function (result) {
                        }
                    });
                }
                else { }
            });
        });

    },

    loadTable: function () {
        datatableviewmodel.AddSearchColumn("dataTable");
        datatableviewmodel.FixHeader();

        module.$dataTable = $("#dataTable").dataTable({
            ajax: {
                "url": baseUrl + "leadstatus/getgridviewdata",
                "type": "POST",
                "data": function (d) {
                    d.code = $("#1").val(),
                    d.name = $("#2").val()
                }
            },
            columns: [
               {
                   "data": "Code",
                   "orderable": true,
               },
               {
                   "data": "Name",
                   "orderable": true
               },
               {
                   "data": "Id",
                   "orderable": false,
                   "render": function (data, type, full, meta) {
                       //return "<a href='/leadstatus/edit/" + data + "' class='btn btn-xs btn-circle blue btn-outline editBtn' title='Edit'>Edit</a><button type='button' class='btn btn-xs btn-circle red btn-outline removeBtn' data-id='" + data + "' title='Remove'>X</button>";
                       return "<div class='actions'>" +
                            "<div class='btn-group'>" +
                            "<a class='btn green btn-outline btn-circle btn-sm' href='javascript:;' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' aria-expanded='true'>" +
                            "<i class='fa fa-angle-down'></i></a>" +
                            "<ul class='dropdown-menu pull-right'>" +
                            "<li><a href='/leadstatus/edit/" + data + "'>Edit</a></li>" +
                            "<li><a type='button' class='removeBtn' data-id='" + data + "' title='Remove'>Delete</a></li>" +
                            //"<li><a href='/leadstatus/edit/" + data + "' class='btn btn-xs btn-circle blue btn-outline editBtn' title='Edit'>Edit</a></li>" +
                            //"<li><a type='button' class='btn btn-xs btn-circle red btn-outline removeBtn' data-id='" + data + "' title='Remove'>Delete</a></li>" +
                            "</ul>";
                   }
               }
            ],
            dom: "frtlip", // horizontal scrollable datatable
            filter: false,
            fixedHeader: {
                header: true,
                headerOffset: datatableviewmodel.fixedHeaderOffset
            },
            language: {
                "lengthMenu": "_MENU_",
                "processing": ""
            },
            lengthMenu: [
                [5, 10, 15, 20, 30],
                [5, 10, 15, 20, 30] // change per page values here
            ],
            order: [[0, "asc"]],
            orderCellsTop: true,
            pageLength: 10,
            paginate: {
                "previous": "Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            },
            pagingType: "bootstrap_full_number",
            processing: true,
            responsive: true,
            serverSide: true,
            stateSave: false
        });

        datatableviewmodel.InitializeEventsForSearchColumn("dataTable", module.$dataTable);
        datatableviewmodel.DropdownFilterDataCountBesideInfo();

        // Reload datatable on search and filter field cleared
        $(".searchColumn").on("input", function (e) {
            if ($(this).val() === "") {
                $("#1").val("");
                $("#2").val("");
                $("#dataTable").DataTable().page.len(10).draw();
            }
        });
    }
}

 
