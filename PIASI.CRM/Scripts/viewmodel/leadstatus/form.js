﻿//$(document).ajaxComplete(function (event, xhr, settings) {
//    console.log("settings.url: " + settings.url);

//    if (settings.url === "ajax/test.html") {
//        $(".log").text("Triggered ajaxComplete handler. The result is " +
//          xhr.responseText);
//    }
//});

$(document).ready(function () {
    module.init();
    module.initEventHandlers();
    module.initFormValidations();
});

var module = {
    isEdit: false,
    originalValues: {},
    leadStatusId: null,
    result: "",

    formIsDirty: function () {
        if (!this.isEdit) { // Add
            if ($("#Code").val() !== "") {
                return true;
            }
            if ($("#Name").val() !== "") {
                return true;
            }

            return false;
        }
        else { // Edit           
            if ($("#Code").val() !== this.originalValues.code) {
                return true;
            }
            if ($("#Name").val() !== this.originalValues.name) {
                return true;
            }

            return false;
        }
    },

    init: function () {
        if (module.result === "OK") {
            //swal("Industry sucessfully saved.", "", "success");
            window.location.href = "/leadstatus";
        }

        commonViewModel.setActiveMenu("management");

        if (this.isEdit) { // Edit Mode
            this.originalValues.code = $("#Code").val();
            this.originalValues.name = $("#Name").val();
        }

        $("#Code").focus();
    },

    initEditorFormValidation: function () {
        var form = $("#editorForm");
        var error = $(".alert-danger", form);
        var success = $(".alert-success", form);

        form.validate({
            errorElement: "span", //default input error message container
            errorClass: "help-block help-block-error", // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                Code: {
                    alphanumeric: true,
                    required: true,
                    unique: true
                },
                Name: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                //App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent(".input-icon").children("i");
                icon.removeClass("fa-check").addClass("fa-warning");
                icon.attr("data-original-title", error.text()).tooltip({ "container": "body" });
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest(".form-input-group").removeClass("has-success").addClass("has-error"); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
            },

            success: function (label, element) {
                var icon = $(element).parent(".input-icon").children("i");
                $(element).closest(".form-input-group").removeClass("has-error").addClass("has-success"); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            }
        });
    },

    initEventHandlers: function () {
        // Create Button
        $("#createBtn").click(function () {
            if ($("#editorForm").valid()) {
                var form = $("#editorForm");
                var error = $(".alert-danger", form);
                var success = $(".alert-success", form);
                success.show();
                error.hide();

                swal({
                    title: "Are you sure?",
                    text: "This will save the new lead status.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // Submit the Form
                        $("#editorForm").submit();
                    }
                    else { }
                });
            }
        });

        // Save Button
        $("#saveBtn").click(function () {
            if ($("#editorForm").valid()) {
                var form = $("#editorLogForm");
                var error = $(".alert-danger", form);
                var success = $(".alert-success", form);
                success.show();
                error.hide();

                swal({
                    title: "Are you sure?",
                    text: "This will update the lead status.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // Submit the Form
                        $("#editorForm").submit();
                    }
                    else { }
                });
            }
        });

        // Cancel Button
        $("#cancelBtn").click(function () {
            if (module.formIsDirty()) {
                swal({
                    title: "Are you sure you want to cancel?",
                    text: "Any unsaved changes will be lost.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "/leadstatus";
                    }
                    else { }
                });
            }
            else {
                window.location.href = "/leadstatus";
            }
        });
    },

    initFormValidations: function () {
        // Alpha-Numeric
        jQuery.validator.addMethod("alphanumeric", function (value, element) {
            // Alpha-Numeric and Underscore
            //return this.optional(element) || /^[\w.]+$/i.test(value);

            // Alpha-Numeric
            return this.optional(element) || /^[a-z0-9]+$/i.test(value);

        }, "Letters and numbers only please.");

        // Unique Code
        jQuery.validator.addMethod("unique", function (value, element) {
            let result = "";
            let isValid = false;
            $.ajax({
                url: "/api/leadstatus/codeisunique/" + value + "/" + module.leadStatusId,
                async: false,
                success: function (data) {
                    result = "SUCCESS";
                    isValid = data;
                },
                error: function () {
                    result = "ERROR";
                    isValid = false;
                }
            });

            while (result === "") { }
            return isValid;

        }, "Code already used.");

        this.initEditorFormValidation();
    }
};