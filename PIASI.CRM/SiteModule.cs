﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using PIASI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIASI.CRM;


namespace PIASI.CRM
{
    public class SiteModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<ConfigSettings>().As<IConfigSettings>().InstancePerLifetimeScope();

            builder.RegisterType<PiasiContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<PiasiContext>().As<IDataContext>().InstancePerLifetimeScope();

           //var global =
           // builder.RegisterControllers(typeof(Global).Assembly).OnActivated(a => a.Context.InjectUnsetProperties(a.Instance)).OnActivating(InjectInvoker);

        }

        private static void InjectInvoker(IActivatingEventArgs<object> obj)
        {
            var invoker = obj.Context.ResolveOptional<IActionInvoker>();
            if (invoker != null)
            {
                ((Controller)obj.Instance).ActionInvoker = invoker;
            }
        }
    }

    
}