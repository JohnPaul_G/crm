﻿using PIASI.Domain.Model;
using System;
using System.Collections.Generic;

namespace PIASI.CRM.Models
{
    public class EmployeeFormViewModel
	{
		public int Id { get; set; }
		public string EmployeeId { get; set; }
        public string EditEmployeeId { get; set; }

        public Boolean IsActive { get; set; }
		
		public string LastName { get; set; }
		
		public string FirstName { get; set; }
		
		public string MiddleName { get; set; }
		
		public string Email { get; set; }
		
		public string ContactNumber { get; set; }
		
		public string Birthdate { get; set; }
		
		public int Position { get; set; }

		public virtual IEnumerable<Position> Positions { get; set; }
		
		public int Department { get; set; }

		public virtual IEnumerable<Department> Departments { get; set; }
		public string Result { get; set; }

		public string Heading { get; set; }

        public bool IsEdit
		{
			get
			{
				return Id>0;
			}
		}

		public string Action
		{
			get
			{
				return IsEdit ? "Edit" : "Add";
			}
		}

	    public string CreatedBy { get; set; }

	    public DateTime CreatedAt { get; set; }
	    public string UpdatedBy { get; set; }

	    public DateTime UpdatedAt { get; set; }
    }
}