﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PIASI.CRM.Models
{
    public class LeadStatusFormViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public string Result { get; set; }

        public string Heading { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedAt { get; set; }

        public bool IsEdit
        {
            get
            {
                return Id > 0;
            }
        }

        public string Action
        {
            get
            {
                return IsEdit ? "Edit" : "Create";
            }
        }
    }
}