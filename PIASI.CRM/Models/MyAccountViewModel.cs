﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PIASI.CRM.Models
{
    public class MyAccountViewModel
    {
        public int UserAccountId { get; set; }
        public string UserEmail { get; set; }
        public string Username { get; set; }
        public int EmployeeId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }

        [Required]
        [DisplayName("Current Password:")]
        public string CurrentPassword { get; set; }

        [Required]
        [DisplayName("New Password:")]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword", ErrorMessage = "Password didn't match!")]
        [DisplayName("Confirm Password:")]
        public string ConfirmPassword { get; set; }
    }
}