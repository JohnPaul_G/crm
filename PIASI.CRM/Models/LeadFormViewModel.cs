﻿using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PIASI.CRM.Models
{
    public class LeadFormViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string MiddleName { get; set; }

        public IEnumerable<LeadPhone> LeadPhones { get; set; }
        public bool DoNotCall { get; set; }
        public string CallAvailability { get; set; }

        public IEnumerable<LeadEmailAddress> EmailAddresses { get; set; }

        public IEnumerable<LeadAssignedUser> AssignedUsers { get; set; }

        [StringLength(300)]
        public string Title { get; set; }

        [StringLength(200)]
        public string Company { get; set; }

        [StringLength(100)]
        public string Website { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string Province { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(20)]
        public string PostalCode { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        public int? LeadStatusId { get; set; }

        public int? IndustryId { get; set; }

        public int? LeadSourceId { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        public string CreatedBy { get; set; }
        
        public string CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public string UpdatedAt { get; set; }

        public string Result { get; set; }

        public string Heading { get; set; }

        public string Name
        {
            get
            {
                return $"{LastName}, {FirstName} {MiddleName}";
            }
        }
        public bool IsEdit
        {
            get
            {
                return Id > 0;
            }
            //get
            //{
            //    return (Heading != "Edit Lead");
            //}
        }

        public string Action
        {
            get
            {
                var result = "";
                if (Heading == "Create Lead")
                {
                    result = "Create";
                }
                else if (Heading == "Edit Lead")
                {
                    result = "Edit";
                }
                else
                {
                    result = "View";
                }

                return result;
            }
        }
    }
}