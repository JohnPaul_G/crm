﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PIASI.CRM.Models
{
    public class UserAccountViewModel
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public string UniqueId { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Password { get; set; }

        public string ActionName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}