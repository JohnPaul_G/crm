﻿using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PIASI.Infrastructure;
using System.Reflection;
//
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Core;
using System.Web.Http;

namespace PIASI.CRM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            Bootstrap();

            // Autofac
            BootstrapAutofac();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        private void BootstrapAutofac()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new SiteModule());
            builder.RegisterModule(new InfrastructureModule());

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //builder.RegisterType<DocumentController>().InstancePerRequest();

            // OPTIONAL: Register the Autofac filter provider.
            //builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            //builder.RegisterWebApiModelBinderProvider();

            // Register all Controller
            builder.RegisterControllers(typeof(MvcApplication).Assembly).OnActivated(a => a.Context.InjectUnsetProperties(a.Instance)).OnActivating(InjectInvoker);

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void InjectInvoker(IActivatingEventArgs<object> obj)
        {
            var invoker = obj.Context.ResolveOptional<IActionInvoker>();
            if (invoker != null)
            {
                ((Controller)obj.Instance).ActionInvoker = invoker;
            }
        }

        private void Bootstrap()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new SiteModule());
            builder.RegisterModule(new InfrastructureModule());
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
