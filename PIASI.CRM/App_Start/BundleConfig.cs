﻿using System.Web;
using System.Web.Optimization;

namespace PIASI.CRM
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/css").Include(
                "~/content/fonts.css",
                "~/content/plugins/font-awesome.min.css",
                "~/content/plugins/simple-line-icons/simple-line-icons.min.css",
                "~/content/css/bootstrap.min.css",
                "~/content/plugins/bootstrap-switch.min.css",
                "~/content/plugins/datatables/datatables.min.css",
                "~/content/plugins/datatables/datatables.bootstrap.css",
                "~/content/plugins/select2/select2.min.css",
                "~/content/plugins/select2/select2-bootstrap.min.css",
                "~/content/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css",
                "~/content/plugins/bootstrap-markdown.min.css",
                "~/content/plugins/sweetalert.css",
                "~/content/plugins/bootstrap-daterangepicker/daterangepicker.min.css",
                "~/content/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css",
                "~/content/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css",
                "~/content/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css",
                "~/content/plugins/bootstrap.touchspin.min.css",
                "~/scripts/plugins/morris/morris.css",
                "~/content/plugins/bootstrap-fileinput.css",
                "~/content/css/components-md.min.css",
                "~/content/css/plugins-md.min.css",
                "~/content/layout/css/layout.min.css",
                "~/content/layout/css/themes/default.min.css",
                "~/content/layout/css/custom.min.css",
                "~/content/layout/css/custom.css"
                ));

            bundles.Add(new ScriptBundle("~/scripts/libs").Include(
                "~/scripts/lib/bootstrap.min.js",
                "~/scripts/lib/js.cookie.min.js",
                "~/scripts/lib/jquery.slimscroll.min.js",
                "~/scripts/lib/app.min.js",
                "~/scripts/lib/jquery.blockui.min.js"
               ));

            bundles.Add(new ScriptBundle("~/scripts/plugins").Include(
                "~/scripts/plugins/bootstrap-switch/bootstrap-switch.min.js",
                "~/scripts/datatable.js",
                "~/scripts/plugins/datatables/datatables.js",
                "~/scripts/plugins/datatables/datatables.bootstrap.js",
                "~/scripts/plugins/select2/select2.full.min.js",
                "~/scripts/plugins/jquery-validation/jquery.validate.min.js",
                "~/scripts/plugins/jquery-validation/additional-methods.min.js",
                "~/scripts/plugins/moment.min.js",
                "~/scripts/plugins/bootstrap-datepicker.min.js",
                "~/scripts/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js",
                "~/scripts/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js",
                "~/scripts/plugins/bootstrap-markdown/markdown.js",
                "~/scripts/plugins/bootstrap-markdown/bootstrap-markdown.js",
                "~/scripts/plugins/bootstrap-sweetalert/sweetalert.min.js",
                "~/scripts/plugins/bootstrap-daterangepicker/daterangepicker.min.js",
                "~/scripts/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js",
                "~/scripts/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js",
                "~/scripts/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js",
                "~/scripts/plugins/bootstrap.touchspin.min.js",
                "~/scripts/plugins/morris/morris.min.js",
                "~/scripts/plugins/morris/raphael-min.js",
                "~/scripts/plugins/bootstrap-fileinput.js",
                "~/scripts/plugins/ckeditor/ckeditor.js",
                "~/scripts/plugins/amcharts/amcharts/amcharts.js",
                "~/scripts/plugins/amcharts/amcharts/serial.js",
                "~/scripts/plugins/amcharts/amcharts/pie.js",
                "~/scripts/plugins/amcharts/amcharts/radar.js",
                "~/scripts/plugins/amcharts/amcharts/themes/light.js",
                "~/scripts/plugins/amcharts/amcharts/themes/patterns.js",
                "~/scripts/plugins/amcharts/themes/chalk.js",
                "~/scripts/plugins/amcharts/ammap/ammap.js",
                "~/scripts/plugins/amcharts/ammap/maps/js/worldLow.js",
                "~/scripts/plugins/underscore-min.js",
                "~/scripts/lib/app.min.js",
                "~/scripts/viewmodel/common/datatable.js",
                "~/scripts/plugins/jquery.timeago.js"));

            bundles.Add(new ScriptBundle("~/scripts/common").Include(
               "~/scripts/viewmodel/common/common-datatable.js",
               "~/scripts/viewmodel/common/common.js",
               "~/scripts/viewmodel/common/input_helper.js"));

            bundles.Add(new ScriptBundle("~/scripts/layouts").Include(
               "~/scripts/layout/layout.min.js",
               "~/scripts/layout/demo.min.js",
               "~/scripts/layout/quick-sidebar.min.js",
               "~/scripts/layout/quick-nav.min.js"
            ));
        }
    }
}
