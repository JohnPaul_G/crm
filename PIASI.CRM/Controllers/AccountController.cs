﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PIASI.CRM.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAccountRepository _accountRepository;
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        // GET: Account
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = UserManager.FindById(userId);
            var userEmployeeId = user.UserAccount.Employee.EmployeeId;
            var userCreatedBy = user.UserAccount.Employee.CreatedBy;
            var userCreatedAt = user.UserAccount.Employee.CreatedAt;
            var userAccountId = user.UserAccountId;
            var userEmail = user.Email;
            var viewModel = new MyAccountViewModel
            {
                UserAccountId = userAccountId,
                UserEmail = userEmail,
                Username = userEmail,
                EmployeeId = userEmployeeId,
                CreatedAt = userCreatedAt,
                CreatedBy = userCreatedBy
            };
            return View(viewModel);
        }

        [HttpPost]
        public async Task<bool> CheckPasswordValidity(string password)
        {
            var userEmail = User.Identity.GetUserName();
            var user = await UserManager.FindByEmailAsync(userEmail);
            var isValid = await SignInManager.UserManager.CheckPasswordAsync(user, password);
            if (isValid)
                return true;

            return false;
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(MyAccountViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                var error = ModelState.Values.SelectMany(v => v.Errors);
                return View("Index", viewModel);
            }

            var user = await UserManager.FindByNameAsync(viewModel.UserEmail);
            var identityID = User.Identity.GetUserId();
            string resetToken = await UserManager.GeneratePasswordResetTokenAsync(identityID);
            var result = await UserManager.ResetPasswordAsync(identityID, resetToken, viewModel.NewPassword);
            if (result.Succeeded)
            {
                var userAccount = new UserAccount()
                {
                    Id = viewModel.UserAccountId,
                    EmployeeId = viewModel.EmployeeId,
                    Username = viewModel.Username,
                    Password = user.PasswordHash,
                    CreatedBy = viewModel.CreatedBy,
                    CreatedAt = viewModel.CreatedAt,
                    UpdatedBy = User.Identity.GetUserId(),
                    UpdatedAt = DateTime.Now
                };

                _accountRepository.InsertOrUpdate(userAccount);
                TempData["ChangePassResponse"] = "Password changed successfully!";
                return RedirectToAction("Index", "Account");
            }
            AddErrors(result);

            return View("Index", viewModel);
        }

        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion

    }
}