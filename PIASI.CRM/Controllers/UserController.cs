﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PIASI.CRM.Controllers
{
    public class UserController : Controller
    {

        private readonly IUserRepository _userRepository;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        
        [Authorize]
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }
        
        //[Route("User/Register?uid={uid}")]
        [HttpGet]
        public ActionResult Register(string uid)
        {
            var userRequest = _userRepository.GetUserRequestByUID(uid);

            if(userRequest == null)
                return HttpNotFound("Invalid link.");

            if (userRequest.ExpirationDate < DateTime.Now)
                return HttpNotFound("The link was already expired.");
            
            var viewModel = new UserAccountViewModel()
            {
                UniqueId = userRequest.UniqueId,
                EmployeeId = userRequest.EmployeeId,
                Email = userRequest.Employee.Email,
                ActionName = "Register"
            };

            return View("UserForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(UserAccountViewModel x)
        {
            if (!ModelState.IsValid)
            {
                return HttpNotFound();
            }
            var hashedPassword = _userRepository.PasswordHash(x.Password);

            var userAccount = new UserAccount()
            {
                EmployeeId = x.EmployeeId,
                Username = x.Email,
                Password = hashedPassword,
                CreatedBy = x.EmployeeId.ToString(),
                CreatedAt = DateTime.Now
            };

            var result = _userRepository.SaveUserAccount(userAccount, x.UniqueId);
            if(result == true)
            {
                var user = new ApplicationUser { UserName = x.Email, Email = x.Email, UserAccountId = userAccount.Id };
                var async = await UserManager.CreateAsync(user, x.Password);
                if (async.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    return RedirectToAction("Index", "Home");
                }
            }            
            return View("UserForm", x);
        }
        
        [HttpPost]
        [Authorize]
        public void DeactivateUserAccount(int empId)
        {
            _userRepository.DeactivateUserAccount(empId);
        }
        
        [HttpPost]
        public bool AjaxResetPassword(int empId)
        {
            var x = _userRepository.SaveAddUserRequest(empId, "ResetPassword");
            return x;
        }

        [HttpGet]
        public ActionResult Reset(string uid)
        {
            var userRequest = _userRepository.GetUserRequestByUID(uid);

            if (userRequest == null)
                return HttpNotFound("Invalid link.");

            var user = _userRepository.GetUserAccount(userRequest.EmployeeId);

            if (userRequest.ExpirationDate < DateTime.Now)
                return HttpNotFound("The link was already expired");

            var viewModel = new UserAccountViewModel()
            {
                Id = user.Id,
                UniqueId = userRequest.UniqueId,
                EmployeeId = userRequest.EmployeeId,
                Email = userRequest.Employee.Email,
                CreatedBy = user.CreatedBy,
                CreatedAt = user.CreatedAt,
                ActionName = "ResetPassword"
            };

            return View("UserForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(UserAccountViewModel x)
        {
            if (!ModelState.IsValid)
            {
                //return View("UserForm",x);
                return Reset(x.UniqueId);
            }
            // For UserAccounts Table
            var userAccount = new UserAccount()
            {
                Id = x.Id,
                EmployeeId = x.EmployeeId,
                Username = x.Email,
                Password = x.Password,
                CreatedBy = x.CreatedBy,
                CreatedAt = x.CreatedAt,
                UpdatedBy = x.Id.ToString(),
                UpdatedAt = DateTime.Now
            };

            // For AspNetUser Table
            var user = await UserManager.FindByNameAsync(x.Email);
            string resetToken = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var result = await UserManager.ResetPasswordAsync(user.Id, resetToken, x.Password);
            if (result.Succeeded)
            {
                _userRepository.InsertOrUpdate(userAccount);
                _userRepository.DeleteUserAccountRequest(x.UniqueId);
                return RedirectToAction("Index", "Home");
            }
            AddErrors(result);

            return Reset(x.UniqueId);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var user = UserManager.FindByEmail(model.Email);

            if(user == null)
                ModelState.AddModelError("", "Email doesn't exist.");
            else
            {
                if (user.UserAccount.IsDeactivated == true)
                {
                    ModelState.AddModelError("", "Your account is deactivated.");
                }
                else
                {
                    var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, false, shouldLockout: false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            Session["LoggedInUser"] = user.UserAccount.Employee.FirstName + " " + user.UserAccount.Employee.LastName;
                            return RedirectToAction("Index", "Home");
                        case SignInStatus.Failure:
                        default:
                            ModelState.AddModelError("", "Password is incorrect.");
                            return View(model);
                    }
                }
            }
            return View(model);
        }
        
        // POST: /Account/LogOff
        [HttpPost]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        #region Grid view
        [HttpPost]
        public ActionResult GetUserGridViewData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            int? employeeid,
            string name,
            string position,
            string department,
            Boolean? isactive
            )
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _userRepository.EmpployeeWithAccountsGridViewData(employeeid, name, position, department, isactive, filter);
            var dataTableData = new DataTableData<UserDto>();
            dataTableData.DataList = data
                .Select(x => new UserDto
                {
                    EmployeeId = x.Employee.Id,
                    Name = x.Employee.LastName + ", " + x.Employee.FirstName + " " + x.Employee.MiddleName,
                    Position = x.Employee.Position.PositionName,
                    Department = x.Employee.Department.DepartmentName,
                    IsActive = x.Employee.IsActive
                })
                .ToList();

            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Employee Without Account Grid View
        [HttpPost]
        public ActionResult GetEmployeeWithoutAccountGridview(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            int? employeeid,
            string name,
            string position,
            string department
        )
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _userRepository.EmpployeeWithoutAccountsGridViewData(employeeid, name, position, department, filter);
            var dataTableData = new DataTableData<UserDto>();
            dataTableData.DataList = data
                .Select(x => new UserDto
                {
                    EmployeeId = x.EmployeeId,
                    Name = x.LastName + ", " + x.FirstName + " " + x.MiddleName,
                    Position = x.Position.PositionName,
                    Department = x.Department.DepartmentName,
                })
                .ToList();

            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
        
    }
}