﻿using Microsoft.AspNet.Identity;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Infrastructure;

namespace PIASI.CRM.Controllers
{
    [Authorize]
    public class LeadSourceController : Controller
    {

        private readonly ILeadSourceRepository _leadSourceRepository;

        public LeadSourceController(ILeadSourceRepository leadSourceRepository)
        {
            _leadSourceRepository = leadSourceRepository;
        }

        #region Grid View
        [HttpPost]
        public ActionResult GetGridViewData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            string code,
            string name
            )
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _leadSourceRepository.GetGridViewData(code, name, filter);
            var dataTableData = new DataTableData<LeadSourceDto>();
            dataTableData.DataList = data
                .Select(x => new LeadSourceDto
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name
                })
                .ToList();
            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: LeadSource
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var vm = new LeadSourceFormViewModel()
            {
                Heading = "Create Lead Source"
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeadSourceFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var leadSource = new LeadSource
                {
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    IsDeleted = false,
                    CreatedBy = User.Identity.GetUserId(), 
                    CreatedAt = DateTime.Now
                };

                _leadSourceRepository.InsertOrUpdate(leadSource);


                if (leadSource.Id > 0)
                {
                    viewModel.Result = "OK";
                }
            }

            return View("Form", viewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var leadSource = _leadSourceRepository.GetLeadSourceById(id);

            var vm = new LeadSourceFormViewModel
            {
                Id = leadSource.Id,
                Heading = "Edit Lead Source",
                Code = leadSource.Code,
                Name = leadSource.Name,
                CreatedBy = leadSource.CreatedBy.ToString(),
                CreatedAt = leadSource.CreatedAt.ToString()
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LeadSourceFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var leadSource = new LeadSource
                {
                    Id = viewModel.Id,
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    CreatedBy = viewModel.CreatedBy,
                    CreatedAt = DateTime.Parse(viewModel.CreatedAt),
                    UpdatedBy = User.Identity.GetUserId(), 
                    UpdatedAt = DateTime.Now 
                };

                _leadSourceRepository.InsertOrUpdate(leadSource);
                viewModel.Result = "OK";

            }

            return View("Form", viewModel);
        }
    }
}
