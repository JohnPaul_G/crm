﻿using Microsoft.AspNet.Identity;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Infrastructure;

namespace PIASI.CRM.Controllers
{
    [Authorize]
    public class LeadStatusController : Controller
    {

        private readonly ILeadStatusRepository _leadStatusRepository;

        public LeadStatusController(ILeadStatusRepository leadStatusRepository)
        {
            _leadStatusRepository = leadStatusRepository;
        }

        #region Grid View
        [HttpPost]
        public ActionResult GetGridViewData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            string code,
            string name
            )
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _leadStatusRepository.GetGridViewData(code, name, filter);
            var dataTableData = new DataTableData<LeadStatusDto>();
            dataTableData.DataList = data
                .Select(x => new LeadStatusDto
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name
                })
                .ToList();
            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: LeadStatus
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var vm = new LeadStatusFormViewModel()
            {
                Heading = "Create Lead Status"
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeadStatusFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var leadStatus = new LeadStatus
                {
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    IsDeleted = false,
                    CreatedBy = User.Identity.GetUserId(), 
                    CreatedAt = DateTime.Now 
                };

                _leadStatusRepository.InsertOrUpdate(leadStatus);


                if (leadStatus.Id > 0)
                {
                    viewModel.Result = "OK";
                }
            }

            return View("Form", viewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var leadStatus = _leadStatusRepository.GetLeadStatusById(id);

            var vm = new LeadStatusFormViewModel
            {
                Id = leadStatus.Id,
                Heading = "Edit Lead Status",
                Code = leadStatus.Code,
                Name = leadStatus.Name,
                CreatedBy = leadStatus.CreatedBy.ToString(),
                CreatedAt = leadStatus.CreatedAt.ToString()
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LeadStatusFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var leadStatus = new LeadStatus
                {
                    Id = viewModel.Id,
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    CreatedBy = viewModel.CreatedBy,
                    CreatedAt = DateTime.Parse(viewModel.CreatedAt),
                    UpdatedBy = User.Identity.GetUserId(), 
                    UpdatedAt = DateTime.Now 
                };

                _leadStatusRepository.InsertOrUpdate(leadStatus);
                viewModel.Result = "OK";
            }

            return View("Form", viewModel);
        }
    }
}