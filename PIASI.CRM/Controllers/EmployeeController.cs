﻿using Microsoft.AspNet.Identity;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PIASI.CRM.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IPositionRepository _positionRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeController(IPositionRepository positionRepository, IDepartmentRepository departmentRepository, IEmployeeRepository employeeRepository)
        {
            _positionRepository = positionRepository;
            _departmentRepository = departmentRepository;
            _employeeRepository = employeeRepository;
        }
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        #region Add Employee
        [HttpGet]
        public ActionResult Add()
        {
            var viewModel = new EmployeeFormViewModel
            {
                Positions = _positionRepository.GetPositions(),
                Departments= _departmentRepository.GetDepartments(),
                Heading= "Create Employee"
            };
            return View("EmployeeForm",viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(EmployeeFormViewModel viewModel)
        {
            var employeeId = Convert.ToInt32(viewModel.EmployeeId);
            if (ModelState.IsValid)
            {
                var employee = new Employee
                {
                    EmployeeId=employeeId,
                    LastName = viewModel.LastName,
                    FirstName=viewModel.FirstName,
                    MiddleName=viewModel.MiddleName,
                    Email = viewModel.Email,
                    ContactNumber = viewModel.ContactNumber,
                    Birthdate = DateTime.Parse(DateTime.Parse(viewModel.Birthdate).Date.ToShortDateString()),
                    PositionId = viewModel.Position,
                    DepartmentId = viewModel.Department,
                    IsActive = true,
                    CreatedBy = User.Identity.GetUserId(),
                    CreatedAt = DateTime.Now,
                    IsDeleted=false
                };

                _employeeRepository.InsertOrUpdate(employee);

                if (employee.Id > 0)
                {
                    viewModel.Result = "OK";
                    viewModel.Positions = _positionRepository.GetPositions();
                    viewModel.Departments = _departmentRepository.GetDepartments();
                    viewModel.Heading = "Create Employee";
                }
            }
            
            return View("EmployeeForm",viewModel);
        }
        #endregion

        #region Edit Employee
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var employee = _employeeRepository.GetEmployeeById(id);
            var viewModel = new EmployeeFormViewModel
            {
                Id=employee.Id,
                EditEmployeeId=employee.EmployeeId.ToString(),
                LastName=employee.LastName,
                FirstName=employee.FirstName,
                MiddleName=employee.MiddleName,
                Email=employee.Email,
                Birthdate=employee.Birthdate.ToShortDateString(),
                ContactNumber=employee.ContactNumber,
                Position=employee.PositionId,
                Department=employee.DepartmentId,
                Positions = _positionRepository.GetPositions(),
                Departments = _departmentRepository.GetDepartments(),
                CreatedBy=employee.CreatedBy,
                CreatedAt=employee.CreatedAt,
                Heading = "Edit Employee"
            };
            return View("EmployeeForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeFormViewModel viewModel)
        {
            var employeeId = Convert.ToInt32(viewModel.EditEmployeeId);

            if (ModelState.IsValid)
            {
                var employee = new Employee
                {
                    Id=viewModel.Id,
                    EmployeeId = employeeId,
                    LastName = viewModel.LastName,
                    FirstName = viewModel.FirstName,
                    MiddleName = viewModel.MiddleName,
                    Email = viewModel.Email,
                    ContactNumber = viewModel.ContactNumber,
                    Birthdate = DateTime.Parse(DateTime.Parse(viewModel.Birthdate).Date.ToShortDateString()),
                    PositionId = viewModel.Position,
                    DepartmentId = viewModel.Department,
                    IsActive = true,
                    CreatedBy=viewModel.CreatedBy,
                    CreatedAt=viewModel.CreatedAt,
                    UpdatedBy = User.Identity.GetUserId(),
                    UpdatedAt = DateTime.Now,
                    IsDeleted = false
                };

                _employeeRepository.InsertOrUpdate(employee);

                if (employee.Id > 0)
                {
                    viewModel.Result = "OK";
                    viewModel.Positions = _positionRepository.GetPositions();
                    viewModel.Departments = _departmentRepository.GetDepartments();
                    viewModel.Heading = "Edit Employee";
                }
            }

            return View("EmployeeForm", viewModel);
        }
        #endregion

        #region DataTable
        [HttpPost]
        public ActionResult GetEmployeeDataTableData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            int? employeeid,
            string name,
            string email,
            string birthdate,
            string contactnumber,
            string position,
            string department)
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _employeeRepository.GetEmployeeDataTableData(employeeid, name, email, birthdate, contactnumber, position, department, filter);
            var dataTableData = new DataTableData<EmployeeDto>();
            dataTableData.DataList = data
                .Select(x => new EmployeeDto
                {
                    Id = x.Id,
                    EmployeeId = x.EmployeeId,
                    Name =x.Name,
                    Email = x.Email,
                    Birthdate = x.Birthdate.ToShortDateString(),
                    ContactNumber = x.ContactNumber,
                    Position = x.Position.PositionName,
                    Department = x.Department.DepartmentName
                })
                .ToList();

            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}