﻿using Microsoft.AspNet.Identity;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Infrastructure;

namespace PIASI.CRM.Controllers
{
    [Authorize]
    public class IndustryController : Controller
    {
        private readonly IIndustryRepository _industryRepository;
        
        public IndustryController(IIndustryRepository industryRepository)
        {
            _industryRepository = industryRepository;
        }

        #region Grid View
        [HttpPost]
        public ActionResult GetGridViewData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            string code,
            string name
            )
        {
            var sortColumn = columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn == null ? "Id" : sortColumn.Replace(" ", String.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _industryRepository.GetGridViewData(code, name, filter);
            var dataTableData = new DataTableData<IndustryDto>();
            dataTableData.DataList = data
                .Select(x => new IndustryDto
                {
                    Id = x.Id,
                    Code = x.Code,
                    Name = x.Name
                })
                .ToList();
            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: Industry
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var vm = new IndustryFormViewModel()
            {
                Heading = "Create Industry"
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IndustryFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var industry = new Industry
                {
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    IsDeleted = false,
                    CreatedBy = User.Identity.GetUserId(), 
                    CreatedAt = DateTime.Now 
                };

                _industryRepository.InsertOrUpdate(industry);


                if (industry.Id > 0)
                {
                    viewModel.Result = "OK";
                }
            }

            return View("Form", viewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var industry = _industryRepository.GetIndustryById(id);

            var vm = new IndustryFormViewModel
            {
                Id = industry.Id,
                Heading = "Edit Industry",
                Code = industry.Code,
                Name = industry.Name,
                CreatedBy = industry.CreatedBy,
                CreatedAt = industry.CreatedAt.ToString()
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IndustryFormViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var industry = new Industry
                {
                    Id = viewModel.Id,
                    Code = viewModel.Code,
                    Name = viewModel.Name,
                    CreatedBy = viewModel.CreatedBy,
                    CreatedAt = DateTime.Parse(viewModel.CreatedAt),
                    UpdatedBy = User.Identity.GetUserId(), //<--- temp
                    UpdatedAt = DateTime.Now //<--- temp
                };

                _industryRepository.InsertOrUpdate(industry);
                viewModel.Result = "OK";

            }

            return View("Form", viewModel);
        }
    }
}