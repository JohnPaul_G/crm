﻿using PIASI.Common.Dtos;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Infrastructure;

namespace PIASI.CRM.Controllers.Api
{
    [Authorize]
    public class LeadController : ApiController
    {
        private readonly ILeadRepository _leadRepository;
        private readonly ILeadAssignedUserRepository _leadAssignedUserRepository;
        private readonly IUserRepository _userRepository;
        
        public LeadController(ILeadRepository leadRepository, ILeadAssignedUserRepository leadAssignedUserRepository, IUserRepository userRepository)
        {
            _leadRepository = leadRepository;
            _leadAssignedUserRepository = leadAssignedUserRepository;
            _userRepository = userRepository;
        }

        [HttpDelete]
        public string Remove(int id)
        {
            return _leadRepository.Remove(id);
        }

        [Route("api/lead/getallleadassigneduserbyleadid/{leadId}")]
        [HttpGet]
        public IEnumerable<LeadAssignedUserDto> GetAllLeadAssignedUserByLeadId(string leadId)
        {
            var repoAssignedUser = _leadAssignedUserRepository.GetAllAssignedUserByLeadId(int.Parse(leadId));
            var repoUserAccount = _userRepository.GetAll()
                .Where(x => x.IsDeactivated == false);
            var data = repoAssignedUser.Select(x => new LeadAssignedUserDto
            {
                Id = x.Id,
                LeadId = x.LeadId,
                UserAccountId = x.UserAccountId,
                EmployeeId = x.UserAccount.EmployeeId,
                EmployeeName = x.UserAccount.Employee.LastName + ", " +
                               x.UserAccount.Employee.FirstName + " " +
                               x.UserAccount.Employee.MiddleName + " ",
                DepartmentName = x.UserAccount.Employee.Department.DepartmentName
            });

            return data;
        }

        [Route("api/lead/getleadassigneduserlistbyleadid/{leadId}")]
        [HttpGet]
        public IEnumerable<LeadAssignedUserDto> GetLeadAssignedUserListByLeadId(string leadId)
        {
            var repoAssignedUser = _leadAssignedUserRepository.GetAllAssignedUserByLeadId(int.Parse(leadId));
            var repoUserAccount = _userRepository.GetAll()
                                   .Where(x => x.IsDeactivated == false);

            var data = repoAssignedUser.Select(x => new LeadAssignedUserDto
            {
                Id = x.Id,
                LeadId = x.LeadId,
                UserAccountId = x.UserAccountId,
                EmployeeId = x.UserAccount.EmployeeId,
                EmployeeName = x.UserAccount.Employee.LastName + ", " +
                               x.UserAccount.Employee.FirstName + " " +
                               x.UserAccount.Employee.MiddleName + " ",
                DepartmentName = x.UserAccount.Employee.Department.DepartmentName


            });
            data = data.Union(repoUserAccount.Select(x => new LeadAssignedUserDto
            {
                UserAccountId = x.Id,
                EmployeeId = x.EmployeeId,
                EmployeeName = x.Employee.LastName + ", " +
                               x.Employee.FirstName + " " +
                               x.Employee.MiddleName,
                DepartmentName = x.Employee.Department.DepartmentName
            }));

            return data;
        }

        [Route("api/lead/getusernamebyuserid/{aspNetUserId}")]
        [HttpGet]
        public ApplicationUser GetUserNameByUserId(string aspNetUserId)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(new PiasiContext()));
            var data = manager.Users.FirstOrDefault(x => x.Id == aspNetUserId);
            return data;
        }
    }
}
