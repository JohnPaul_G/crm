﻿using PIASI.Common.Dtos;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PIASI.CRM.Controllers.Api
{
    [Authorize]
    public class LeadStatusController : ApiController
    {
        private readonly ILeadStatusRepository _leadStatusRepository;

        public LeadStatusController(ILeadStatusRepository leadStatusRepository)
        {
            _leadStatusRepository = leadStatusRepository;
        }

        [HttpGet]
        [Route("api/leadstatus/codeisunique/{code}/{id?}")]
        public bool CodeIsUnique(string code, int id = 0)
        {
            return !_leadStatusRepository.CodeExists(code, id);
        }

        [HttpGet]
        [Route("api/leadstatus/getleadstatuslist")]
        public IEnumerable<LeadStatusDto> GetList()
        {
            var leadStatuses = _leadStatusRepository.GetAll();
            var leadStatusDto = leadStatuses.Select(x => new LeadStatusDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name
            });

            return leadStatusDto;
        }

        [HttpDelete]
        //public string Remove(int id)
        //{
        //    return _leadStatusRepository.Remove(id);
        //}

        [HttpGet]
        [Route("api/leadstatus/getleadstatusbyid/{id}")]
        public LeadStatusDto GetLeadStatusById(int id = 0)
        {
            var leadStatus = _leadStatusRepository.GetLeadStatusById(id);
            if (leadStatus == null)
            {
                return null;
            }
            var data = new LeadStatusDto()
            {
                Id = leadStatus.Id,
                Name = leadStatus.Name,
                Code = leadStatus.Code
            };

            return data;
        }

    }
}
