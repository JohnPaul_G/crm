﻿using Microsoft.AspNet.Identity;
using PIASI.Domain.Repository;
using System;
using System.Web.Http;

namespace PIASI.CRM.Controllers.Api
{
    public class EmployeeController : ApiController
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet]
        [Route("api/employee/idexists/{employeeid}")]
        public bool IdExists(int employeeid)
        {
            return !_employeeRepository.IdExists(employeeid);
        }

        [HttpDelete]
        public string Remove(int id)
        {
            try
            {
                var employee = _employeeRepository.GetEmployeeById(id);
                employee.IsDeleted = true;
                employee.IsActive = false;
                employee.UpdatedBy = User.Identity.GetUserId();
                employee.UpdatedAt = DateTime.Now;

                _employeeRepository.InsertOrUpdate(employee);
                return "OK";
            }
            catch (Exception)
            {

                return "ERROR";
            }
        }
    }
}
