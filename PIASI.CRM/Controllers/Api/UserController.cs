﻿using PIASI.Common.Dtos;
using PIASI.Domain.Repository;
using System.Collections.Generic;
using System.Web.Http;

namespace PIASI.CRM.Controllers.Api
{
    public class UserController : ApiController
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        public List<SendEmailResultDto> SendAddUserRequestEmail(int[] empIds)
        {
            var emailResults = new List<SendEmailResultDto>();

            foreach (int empId in empIds)
            {
                var x = _userRepository.SaveAddUserRequest(empId, "Register");
                emailResults.Add(new SendEmailResultDto()
                {
                    EmpId = empId,
                    Result = x
                });

            }
            //return Ok("Email Sent!");

            return emailResults;

        }
    }
}
;