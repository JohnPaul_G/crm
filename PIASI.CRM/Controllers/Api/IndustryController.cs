﻿using PIASI.Common.Dtos;
using PIASI.Domain.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PIASI.CRM.Controllers.Api
{
    [Authorize]
    public class IndustryController : ApiController
    {
        private readonly IIndustryRepository _industryIndustry;

        public IndustryController(IIndustryRepository industryRepository)
        {
            _industryIndustry = industryRepository;
        }

        [HttpGet]
        [Route("api/industry/codeisunique/{code}/{id?}")]
        public bool CodeIsUnique(string code, int id = 0)
        {
            return !_industryIndustry.CodeExists(code, id);
        }

        [HttpGet]
        [Route("api/industry/getindustrylist")]
        public IEnumerable<IndustryDto> GetList()
        {
            var industries = _industryIndustry.GetAll();
            var industryDto = industries.Select(x => new IndustryDto
            {
                Id = x.Id,
                Name = x.Name
            });
            return industryDto;
        }

        [HttpDelete]
        //public string Remove(int id)
        //{
        //    return _industryIndustry.Remove(id);
        //}

        [HttpGet]
        [Route("api/industry/getindustrybyid/{id}")]
        public IndustryDto GetIndustryById(int id = 0)
        {
            var industry = _industryIndustry.GetIndustryById(id);
            if (industry == null)
            {
                return null;
            }
            var data = new IndustryDto()
            {
                Id = industry.Id,
                Name = industry.Name,
                Code = industry.Code
            };

            return data;
        }
    }
}
