﻿using PIASI.Common.Dtos;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PIASI.CRM.Controllers.Api
{
    [Authorize]
    public class LeadSourceController : ApiController
    {
        private readonly ILeadSourceRepository _leadSourceRepository;

        public LeadSourceController(ILeadSourceRepository leadSourceRepository)
        {
            _leadSourceRepository = leadSourceRepository;
        }

        [HttpGet]
        [Route("api/leadsource/codeisunique/{code}/{id?}")]
        public bool CodeIsUnique(string code, int id = 0)
        {
            return !_leadSourceRepository.CodeExists(code, id);
        }

        [HttpGet]
        [Route("api/leadsource/getleadsourcelist")]
        public IEnumerable<LeadSourceDto> GetList()
        {
            var leadSource = _leadSourceRepository.GetAll();
            var leadSourceDto = leadSource.Select(x => new LeadSourceDto
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name
            });

            return leadSourceDto;
        }

        [HttpDelete]
        //public string Remove(int id)
        //{
        //    return _leadSourceRepository.Remove(id);
        //}

        [HttpGet]
        [Route("api/leadsource/getleadsourcebyid/{id}")]
        public LeadSourceDto GetLeadSourceById(int id = 0)
        {
            var leadSource = _leadSourceRepository.GetLeadSourceById(id);
            if (leadSource == null)
            {
                return null;
            }
            var data = new LeadSourceDto
            {
                Id = leadSource.Id,
                Name = leadSource.Name,
                Code = leadSource.Code
            };

            return data;
        }
    }
}
