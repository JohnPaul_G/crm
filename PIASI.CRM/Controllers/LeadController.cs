﻿using Microsoft.AspNet.Identity;
using PIASI.Common.Dtos;
using PIASI.Common.Entities;
using PIASI.CRM.Models;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Infrastructure;

namespace PIASI.CRM.Controllers
{
    [Authorize]
    public class LeadController : Controller
    {
        private readonly ILeadRepository _leadRepository;
        private readonly ILeadAssignedUserRepository _leadAssignedUserRepository;
        private readonly ILeadEmailAddressRepository _leadEmailAddressRepository;
        private readonly ILeadPhoneRepository _leadPhoneRepository;

        public LeadController(
            ILeadRepository leadRepository,
            ILeadAssignedUserRepository leadAssignedUserRepository,
            ILeadPhoneRepository leadPhoneRepository,
            ILeadEmailAddressRepository leadEmailAddressRepository
            )
        {
            _leadRepository = leadRepository;
            _leadAssignedUserRepository = leadAssignedUserRepository;
            _leadPhoneRepository = leadPhoneRepository;
            _leadEmailAddressRepository = leadEmailAddressRepository;
        }

        #region Grid View
        [HttpPost]
        public ActionResult GetGridViewData(
            int draw,
            int start,
            int length,
            List<Dictionary<string, string>> columns,
            List<Dictionary<string, string>> order,
            string name,
            string company,
            string email,
            string website,
            string assignedUser,
            string createdAt
            )
        {
            var sortColumn = "Id"; //columns[ConvertHelper.ToInteger(order[0]["column"])]["data"];
            var filter = new DataFilter
            {
                Start = start,
                Length = length,
                SortColumn = sortColumn.Replace(" ", string.Empty),
                SortDirection = order[0]["dir"],
                ColumnFilters = new List<ColumnFilter>()
            };

            var data = _leadRepository.GetGridViewData(name, company, email, website, assignedUser, createdAt, filter);

            var dataTableData = new DataTableData<LeadDto>();
            dataTableData.DataList = data
                .Select(x => new LeadDto
                {
                    Id = x.Id,
                    Name = $"{x.LastName}, {x.FirstName} {x.MiddleName}",
                    Company = x.Company,
                    Email = x.EmailAddresses.Select(c => c.Name).FirstOrDefault(),
                    Website = x.Website,
                    //AssignedUser = _leadAssignedUserRepository.GetAllAssignedUserByLeadId(x.Id)
                    //                                          .Select(y => (
                    //                                                  y.UserAccount.Employee.LastName + ", " +
                    //                                                  y.UserAccount.Employee.FirstName + " " +
                    //                                                  y.UserAccount.Employee.MiddleName))
                    //                                          .FirstOrDefault(),
                    AssignedUser = x.AssignedUsers.Select(y => (
                            y.UserAccount.Employee.LastName + ", " +
                            y.UserAccount.Employee.FirstName + " " +
                            y.UserAccount.Employee.MiddleName))
                        .FirstOrDefault(),
                    CreatedAt = $"{x.CreatedAt:MMM-dd yyyy ddd}"
                })
                .ToList();
            dataTableData.Draw = draw;
            dataTableData.RecordsTotal = filter.TotalRecordCount;
            dataTableData.RecordsFiltered = filter.FilteredRecordCount;

            return this.Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: Lead
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult View(int id)
        {
            var vm = LeadViewModel(id, "View Lead");

            return View("View", vm);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var vm = new LeadFormViewModel()
            {
                Heading = "Create Lead"
            };

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            LeadFormViewModel viewModel,
            //int? leadStatusList, //dropdown lead status
            //int? leadSourceList,// dropdown lead source
            //int? industryList, //dropdown industry
            IEnumerable<string> leadPhoneList, //list of Lead phones
            IEnumerable<string> leadEmailList, //list of Lead Emails
            IEnumerable<string> leadAssignedUserList //list of Lead Assigned users
            )
        {
            if (ModelState.IsValid)
            {
                var lead = new Lead
                {
                    LastName = viewModel.LastName,
                    FirstName = viewModel.FirstName,
                    MiddleName = viewModel.MiddleName,
                    CallAvailability = viewModel.CallAvailability,
                    DoNotCall = viewModel.DoNotCall,
                    Title = viewModel.Title,
                    Company = viewModel.Company,
                    Website = viewModel.Website,
                    Street = viewModel.Street,
                    Province = viewModel.Province,
                    City = viewModel.City,
                    PostalCode = viewModel.PostalCode,
                    Country = viewModel.Country,
                    LeadStatusId = (viewModel.LeadStatusId <= 0) ? null : viewModel.LeadStatusId, // (leadStatusList <= 0) ? null : leadStatusList, 
                    LeadSourceId = (viewModel.LeadSourceId <= 0) ? null : viewModel.LeadSourceId, // (leadSourceList <= 0) ? null : leadSourceList, 
                    IndustryId = (viewModel.IndustryId <= 0) ? null : viewModel.IndustryId, // (industryList <= 0) ? null : industryList, 
                    Description = viewModel.Description,
                    IsDeleted = false,
                    CreatedBy = User.Identity.GetUserId(), //<--- temp
                    CreatedAt = DateTime.Now //<--- temp
                };

                //_leadRepository.Add(lead);
                _leadRepository.InsertOrUpdate(lead);
                if (lead.Id > 0)
                {
                    // save details

                    #region save LeadPhone
                    var leadPhones = GetLeadPhonesDropDown(leadPhoneList, lead.Id);
                    _leadPhoneRepository.Add(leadPhones);
                    #endregion

                    #region Save Lead Email Addresses
                    var leadEmailAddresses = GetEmailAddressesDropDown(leadEmailList, lead.Id);
                    _leadEmailAddressRepository.Add(leadEmailAddresses);
                    #endregion

                    #region save lead AssignedUser
                    var leadAssignedUsers = GetLeadAssignedUsersDropDown(leadAssignedUserList, lead.Id);
                    _leadAssignedUserRepository.Add(leadAssignedUsers);
                    #endregion

                    viewModel.Result = "OK";
                }
            }

            return View("Form", viewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var vm = LeadViewModel(id, "Edit Lead");

            return View("Form", vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            LeadFormViewModel vm,
            //int? leadStatusList, //drop downstatus
            //int? leadSourceList,// dropdown lead source
            //int? industryList, //dropdown industry
            IEnumerable<string> leadPhoneList, //list of Lead phones
            IEnumerable<string> leadEmailList, //list of Lead Emails
            IEnumerable<string> leadAssignedUserList //list of Lead Assigned users
            )
        {
            if (ModelState.IsValid)
            {
                var lead = new Lead
                {
                    Id = vm.Id,
                    LastName = vm.LastName,
                    FirstName = vm.FirstName,
                    MiddleName = vm.MiddleName,
                    CallAvailability = vm.CallAvailability,
                    DoNotCall = vm.DoNotCall,
                    Title = vm.Title,
                    Company = vm.Company,
                    Website = vm.Website,
                    Street = vm.Street,
                    Province = vm.Province,
                    City = vm.City,
                    PostalCode = vm.PostalCode,
                    Country = vm.Country,
                    LeadStatusId = (vm.LeadStatusId <= 0) ? null : vm.LeadStatusId, //(leadStatusList <= 0) ? null : leadStatusList,
                    LeadSourceId = (vm.LeadSourceId <= 0) ? null : vm.LeadSourceId, //(leadSourceList <= 0) ? null : leadSourceList, 
                    IndustryId = (vm.IndustryId <= 0) ? null : vm.IndustryId, //(industryList <= 0) ? null : industryList, 
                    Description = vm.Description,
                    CreatedAt = DateTime.Parse(vm.CreatedAt),
                    CreatedBy = vm.CreatedBy,
                    UpdatedBy = User.Identity.GetUserId(),
                    UpdatedAt = DateTime.Now
                };

                // save details
                #region Save Lead Phones 
                var leadPhones = GetLeadPhonesDropDown(leadPhoneList, lead.Id);
                _leadPhoneRepository.UpdateByLeadId(leadPhones, lead.Id);
                #endregion

                #region Save Lead Email Addresses
                var leadEmailAddresses = GetEmailAddressesDropDown(leadEmailList, lead.Id);
                _leadEmailAddressRepository.UpdateByLeadId(leadEmailAddresses, lead.Id);
                #endregion

                #region save lead AssignedUser
                var leadAssignedUsers = GetLeadAssignedUsersDropDown(leadAssignedUserList, lead.Id);
                _leadAssignedUserRepository.UpdateByLeadId(leadAssignedUsers, lead.Id);
                #endregion

                //_leadRepository.Update(lead);
                _leadRepository.InsertOrUpdate(lead);
                vm.Result = "OK";
            }

            return View("Form", vm);
        }

        private LeadFormViewModel LeadViewModel(int id, string heading)
        {
            var leadRepo = _leadRepository.GetLeadById(id);
            var vm = new LeadFormViewModel
            {
                Id = leadRepo.Id,
                LastName = leadRepo.LastName,
                FirstName = leadRepo.FirstName,
                MiddleName = leadRepo.MiddleName,
                LeadPhones = leadRepo.LeadPhones,
                CallAvailability = leadRepo.CallAvailability,
                DoNotCall = leadRepo.DoNotCall,
                AssignedUsers = leadRepo.AssignedUsers, // _leadAssignedUserRepository.GetAllAssignedUserByLeadId(id).ToList(),
                EmailAddresses = leadRepo.EmailAddresses,
                Title = leadRepo.Title,
                Company = leadRepo.Company,
                Website = leadRepo.Website,
                Street = leadRepo.Street,
                Province = leadRepo.Province,
                City = leadRepo.City,
                PostalCode = leadRepo.PostalCode,
                Country = leadRepo.Country,
                LeadStatusId = leadRepo.LeadStatusId,
                IndustryId = leadRepo.IndustryId,
                LeadSourceId = leadRepo.LeadSourceId,
                Description = leadRepo.Description,
                CreatedAt = leadRepo.CreatedAt.ToString(),
                CreatedBy = leadRepo.CreatedBy,
                UpdatedAt = leadRepo.UpdatedAt.ToString(),
                UpdatedBy = leadRepo.UpdatedBy,
                Heading = heading
            };

            return vm;
        }

        private List<LeadAssignedUser> GetLeadAssignedUsersDropDown(IEnumerable<string> leadAssignedUserList, int leadId)
        {
            var leadAssignedUsers = new List<LeadAssignedUser>();
            if (leadAssignedUserList != null)
            {
                foreach (var item in leadAssignedUserList)
                {
                    leadAssignedUsers.Add(new LeadAssignedUser
                    {
                        LeadId = leadId,
                        UserAccountId = int.Parse(item)
                    });
                }
            };

            return leadAssignedUsers;
        }

        private List<LeadEmailAddress> GetEmailAddressesDropDown(IEnumerable<string> leadEmailList, int leadId)
        {
            var leadEmailAddresses = new List<LeadEmailAddress>();
            if (leadEmailList != null)
            {
                foreach (var item in leadEmailList)
                {
                    string[] email = item.Split('-'); // email[0] = Id // email[1] = Email name

                    leadEmailAddresses.Add(new LeadEmailAddress
                    {
                        LeadEmailAddressId = int.Parse(email[0]),
                        LeadId = leadId,
                        Name = item.Substring(item.IndexOf('-', 1) + 1) // email[1]
                    });
                }
            };

            return leadEmailAddresses;
        }

        private List<LeadPhone> GetLeadPhonesDropDown(IEnumerable<string> leadPhoneList, int leadId)
        {
            var leadPhones = new List<LeadPhone>();
            if (leadPhoneList != null)
            {
                foreach (var item in leadPhoneList)
                {
                    string[] phoneNo = item.Split('-'); // phone[0] = Id // phone[1] = Type // phone[2] + ... = Phone number
                    leadPhones.Add(new LeadPhone
                    {
                        Id = int.Parse(phoneNo[0]),
                        LeadId = leadId,
                        PhoneNo = phoneNo[2], // item.Substring(item.IndexOf('-', 2) + 1),
                        IsPrimary = true,
                        Type = phoneNo[1]
                    });
                }
            };

            return leadPhones;
        }
    }
}