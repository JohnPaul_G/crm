﻿using System.ComponentModel.DataAnnotations;

namespace PIASI.Domain.Model
{
    public class UserAccount : ModelBase
    {
        [Required]
        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        [Required]
        [StringLength(150)]
        public string Username { get; set; }

        [Required]
        [StringLength(150)]
        public string Password { get; set; }

        public bool IsDeactivated { get; set; }
    }
}
