﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Model
{
    public class LeadPhone
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Lead")]
        public int LeadId { get; set; }
        public virtual Lead Lead { get; set; }

        [Required]
        [StringLength(11, MinimumLength = 6)]
        public string PhoneNo { get; set; }

        public bool IsPrimary { get; set; }

        [Required]
        public string Type { get; set; }
    }
}
