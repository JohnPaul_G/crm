﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Model
{
    public class Lead : ModelBase
    { 
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string MiddleName { get; set; }

        [StringLength(300)]
        public string Title { get; set; }

        [StringLength(200)]
        public string Company { get; set; }

        [StringLength(100)]
        public string Website { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string Province { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(20)]
        public string PostalCode { get; set; }

        [StringLength(100)]
        public string Country { get; set; }

        [ForeignKey("LeadStatus")]
        public int? LeadStatusId { get; set; }
        public virtual LeadStatus LeadStatus { get; set; }

        [ForeignKey("LeadSource")]
        public int? LeadSourceId { get; set; }
        public virtual LeadSource LeadSource { get; set; }

        [ForeignKey("Industry")]
        public int? IndustryId { get; set; }
        public virtual Industry Industry { get; set; }

        public bool DoNotCall { get; set; }

        [StringLength(120)]
        public string CallAvailability { get; set; }

        [StringLength(300)]
        public string Description { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public virtual ICollection<LeadPhone> LeadPhones { get; set; }

        public virtual ICollection<LeadAssignedUser> AssignedUsers { get; set; }

        public virtual ICollection<LeadEmailAddress> EmailAddresses { get; set; }
    }
}
