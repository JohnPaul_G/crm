﻿using System.ComponentModel.DataAnnotations;

namespace PIASI.Domain.Model
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }

        [StringLength(150)]
        public string DepartmentName { get; set; }
    }
}