﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PIASI.Domain.Model
{
    public class CreateUserRequest
    {
        [Key]
        [Required]
        public string UniqueId { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public DateTime ExpirationDate { get; set; }

        [Required]
        public string RequestType { get; set; }
        
    }
}
