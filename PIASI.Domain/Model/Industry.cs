﻿using System.ComponentModel.DataAnnotations;

namespace PIASI.Domain.Model
{
    public class Industry : ModelBase
    {
        [MaxLength(50)]
        [Required]
        public string Code { get; set; }

        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        [Required]
        public bool IsDeleted { get; set; }
    }
}
