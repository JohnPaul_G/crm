﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PIASI.Domain.Model
{
    public class ModelBase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}
