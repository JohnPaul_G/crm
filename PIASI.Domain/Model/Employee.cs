﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PIASI.Domain.Model
{
    public class Employee:ModelBase
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public Boolean IsActive { get; set; }

        [Required]
        [StringLength(150)]
        public string LastName { get; set; }

        [Required]
        [StringLength(150)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(150)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string ContactNumber { get; set; }

        [Required]
        public DateTime Birthdate { get; set; }

        [ForeignKey("Position")]
        public int PositionId { get; set; }

        public virtual Position Position { get; set; }

        [ForeignKey("Department")]
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        
        [Required]
        public Boolean IsDeleted { get; set; }

        [NotMapped]
        public string Name
        {
            get
            {
                return $"{LastName}, {FirstName} {MiddleName}";
            }
        }
    }
}
