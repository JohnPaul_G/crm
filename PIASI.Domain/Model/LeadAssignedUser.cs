﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Model
{
    public class LeadAssignedUser
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Lead")]
        public int LeadId { get; set; }
        public virtual Lead Lead { get; set; }

        [ForeignKey("UserAccount")]
        public int UserAccountId { get; set; }
        public virtual UserAccount UserAccount { get; set; }
    }
}
