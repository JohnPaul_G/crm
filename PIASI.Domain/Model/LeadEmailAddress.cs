﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Model
{
    public class LeadEmailAddress
    {
        [Key]
        public int LeadEmailAddressId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        
        [ForeignKey("Lead")]
        public int LeadId { get; set; }
        public virtual Lead Lead { get; set; }
    }
}
