﻿using System.ComponentModel.DataAnnotations;

namespace PIASI.Domain.Model
{
    public class Position
    {
        [Key]
        public int PositionId { get; set; }

        [StringLength(150)]
        public string PositionName { get; set; }
    }
}