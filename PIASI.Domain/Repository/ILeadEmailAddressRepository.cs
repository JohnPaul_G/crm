﻿using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface ILeadEmailAddressRepository
    {
        List<LeadEmailAddress> Add(List<LeadEmailAddress> leadEmailAddresses);

        IEnumerable<LeadEmailAddress> GetAll(int leadId);

        Tuple<bool, string> UpdateByLeadId(List<LeadEmailAddress> leadEmailAddresses, int leadId);
    }
}
