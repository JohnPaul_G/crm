﻿using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface ILeadAssignedUserRepository
    {
        IEnumerable<LeadAssignedUser> GetAllAssignedUserByLeadId(int leadId);

        List<LeadAssignedUser> Add(List<LeadAssignedUser> leadAssignedUsers);

        Tuple<bool, string> UpdateByLeadId(List<LeadAssignedUser> leadAssignedUsers, int leadId);
    }
}
