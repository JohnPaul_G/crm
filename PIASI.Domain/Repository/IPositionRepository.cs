﻿using PIASI.Domain.Model;
using System.Collections.Generic;

namespace PIASI.Domain.Repository
{
    public interface IPositionRepository:IRepositoryBase
    {
        IEnumerable<Position> GetPositions();
    }
}
