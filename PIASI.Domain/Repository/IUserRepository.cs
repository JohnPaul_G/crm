﻿using PIASI.Common.Entities;
using PIASI.Domain.Model;
using System;
using System.Collections.Generic;

namespace PIASI.Domain.Repository
{
    public interface IUserRepository : IRepositoryBase
    {
        IEnumerable<UserAccount> EmpployeeWithAccountsGridViewData(int? empId, string name, string position, string department, Boolean? isactive, DataFilter filter);
        IEnumerable<Employee> EmpployeeWithoutAccountsGridViewData(int? empId, string lastname, string position, string department, DataFilter filter);
        bool SaveAddUserRequest(int empId, string requestType);
        CreateUserRequest GetUserRequestByUID(string uid);
        bool SaveUserAccount(UserAccount userAccount, string uid);
        void DeleteUserAccountRequest(string uid);
        IEnumerable<UserAccount> GetAll();
        void DeactivateUserAccount(int empId);
        UserAccount GetUserAccount(int empId);
        string PasswordHash(string password);
    }
}
