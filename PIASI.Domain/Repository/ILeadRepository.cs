﻿using PIASI.Common.Entities;
using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface ILeadRepository : IRepositoryBase
    {
        Lead Add(Lead lead);

        bool CodeExists(int id = 0);

        IEnumerable<Lead> GetAll();

        Lead GetLeadById(int id);

        IEnumerable<Lead> GetGridViewData(string name, string company,string email, string website, string assignedUser, string createdAt, DataFilter filter);

        string Remove(int id);

        Lead Update(Lead lead);

    }
}
