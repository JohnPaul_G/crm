﻿using PIASI.Common.Entities;
using PIASI.Domain.Model;
using System.Collections.Generic;

namespace PIASI.Domain.Repository
{
    public interface IEmployeeRepository:IRepositoryBase
    {
        Employee GetEmployeeById(int id);
        //void AddEmployee(Employee employee);

        bool IdExists(int employeeid);
        
        IEnumerable<Employee> GetEmployeeDataTableData(
            int? employeeid,
            string name,
            string email,
            string birthdate,
            string contactnumber,
            string position,
            string department,
            DataFilter filter);

    }
}
