﻿using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface ILeadPhoneRepository
    {
        List<LeadPhone> Add(List<LeadPhone> leadPhones);

        IEnumerable<LeadPhone> GetAll();

        string RemoveByLeadId(int leadId);

        Tuple<bool, string> UpdateByLeadId(List<LeadPhone> leadPhones, int leadId);
    }
}
