﻿using PIASI.Domain.Model;

namespace PIASI.Domain.Repository
{
    public interface IRepositoryBase
    {
        void InsertOrUpdate(ModelBase entity);
        int SaveChanges();
    }
}
