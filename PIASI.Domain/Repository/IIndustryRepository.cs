﻿using PIASI.Common.Entities;
using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface IIndustryRepository : IRepositoryBase
    {
        bool CodeExists(string code, int id = 0);

        IEnumerable<Industry> GetAll();

        Industry GetIndustryById(int id);

        IEnumerable<Industry> GetGridViewData(string code, string name, DataFilter filter);

        //string Remove(int id);
    }
}
