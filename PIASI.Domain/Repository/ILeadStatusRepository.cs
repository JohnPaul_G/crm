﻿using PIASI.Common.Entities;
using PIASI.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Domain.Repository
{
    public interface ILeadStatusRepository : IRepositoryBase
    {
        bool CodeExists(string code, int id = 0);

        IEnumerable<LeadStatus> GetAll();

        LeadStatus GetLeadStatusById(int id);

        IEnumerable<LeadStatus> GetGridViewData(string code, string name, DataFilter filter);

        //string Remove(int id);
    }
}
