﻿using PIASI.Domain.Model;
using System.Collections.Generic;

namespace PIASI.Domain.Repository
{
    public interface IDepartmentRepository:IRepositoryBase
    {
        IEnumerable<Department> GetDepartments();
    }
}
