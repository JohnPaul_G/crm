﻿using Microsoft.AspNet.Identity.EntityFramework;
using PIASI.Domain.Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace PIASI.Infrastructure
{
    public class PiasiContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        #region Fields

        private const string ConnectionString = "PiasiDBContext";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PiasiContext"/> class.
        /// </summary>
        public PiasiContext()
            : base(ConnectionString)
        {
            //Configuration.ProxyCreationEnabled = false;
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = 1200;
        }
        //static PnpPayrollContext()
        //{
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<PnpPayrollContext, Configuration>());
        //}
        /// <summary>
        /// Initializes a new instance of the <see cref="PiasiDBContext"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public PiasiContext(string connectionString)
            : base(connectionString)
        {
            //Configuration.ProxyCreationEnabled = false;          

            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = 1200;
        }

        #endregion

        #region DbContext Events

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<EdatsDocument>().with(m=>m.EdatsStatus).
            //modelBuilder.Entity<EdatsDocument>()
            //    .HasOptional(m => m.EdatsStatus)
            //    .WithMany()
            //    .HasForeignKey(m => m.iStatusId);

            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            //modelBuilder.Conventions.Add<CascadeDeleteAttributeConvention>();
        }

        #endregion

        #region DbSet Fields

        public IDbSet<Industry> Industries { get; set; }
        public IDbSet<Employee> Employees { get; set; }
        public IDbSet<Position> Positions { get; set; }
        public IDbSet<Department> Departments { get; set; }
        public IDbSet<LeadStatus> LeadStatuses { get; set; }
        public IDbSet<LeadSource> LeadSources { get; set; }
        public DbSet<LeadEmailAddress> LeadEmailAddresses { get; set; }
        public DbSet<LeadPhone> LeadPhones { get; set; }
        public DbSet<LeadAssignedUser> LeadAssignedUsers { get; set; }
        public IDbSet<Lead> Leads { get; set; }
        public IDbSet<UserAccount> UserAccounts { get; set; }
        public IDbSet<CreateUserRequest> CreateUserRequests { get; set; }
        #endregion

        void IDataContext.SaveChanges()
        {
            try
            {
                base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }

                throw;  // You can also choose to handle the exception here...
            }
        }

        public static PiasiContext Create()
        {
            return new PiasiContext();
        }
    }
}
