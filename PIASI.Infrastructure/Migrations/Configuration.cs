
namespace PIASI.Infrastructure.Migrations
{
    using Domain.Model;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<PIASI.Infrastructure.PiasiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PIASI.Infrastructure.PiasiContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            CreateTestData(context);
        }

        public void CreateTestData(PiasiContext context)
        {
            context.Database.ExecuteSqlCommand("Delete from Lead");
            context.Database.ExecuteSqlCommand("Delete from LeadAssignedUser");
            context.Database.ExecuteSqlCommand("Delete from LeadEmailAddress");
            context.Database.ExecuteSqlCommand("Delete from LeadPhone");
            context.Database.ExecuteSqlCommand("Delete from LeadSource");
            context.Database.ExecuteSqlCommand("Delete from LeadStatus");

            //Employees
            context.Employees.AddOrUpdate(e => e.EmployeeId, new Employee() { EmployeeId = 1, FirstName = "Juan", MiddleName = "Dee", LastName = "Dela Cruz", Email = "juan@gmail.com", ContactNumber = "09051234567", Birthdate = DateTime.Parse("01/01/2001"), PositionId = 1, DepartmentId = 1, IsActive = true, IsDeleted = false, CreatedAt = DateTime.Now, CreatedBy = "1" });
            context.Employees.AddOrUpdate(e => e.EmployeeId, new Employee() { EmployeeId = 2, FirstName = "Juana", MiddleName = "Deezon", LastName = "Dela Cruza", Email = "juana@gmail.com", ContactNumber = "09123456789", Birthdate = DateTime.Parse("02/02/2002"), PositionId = 2, DepartmentId = 1, IsActive = true, IsDeleted = false, CreatedAt = DateTime.Now, CreatedBy = "2" });
            context.Employees.AddOrUpdate(e => e.EmployeeId, new Employee() { EmployeeId = 3, FirstName = "Juancho", MiddleName = "Yu", LastName = "Dee", Email = "juancho@gmail.com", ContactNumber = "09051234567", Birthdate = DateTime.Parse("03/03/2003"), PositionId = 3, DepartmentId = 3, IsActive = true, IsDeleted = false, CreatedAt = DateTime.Now, CreatedBy = "3" });
            context.Employees.AddOrUpdate(e => e.EmployeeId, new Employee() { EmployeeId = 4, FirstName = "John Paul", MiddleName = "Pacardo", LastName = "Greganda", Email = "johnpaulgreganda@gmail.com", ContactNumber = "09051234567", Birthdate = DateTime.Parse("03/03/2003"), PositionId = 3, DepartmentId = 3, IsActive = true, IsDeleted = false, CreatedAt = DateTime.Now, CreatedBy = "4" });

            //Positions
            context.Positions.AddOrUpdate(p => p.PositionId, new Position() { PositionId = 1, PositionName = "Sales Agent" });
            context.Positions.AddOrUpdate(p => p.PositionId, new Position() { PositionId = 2, PositionName = "Sales Manager" });
            context.Positions.AddOrUpdate(p => p.PositionId, new Position() { PositionId = 3, PositionName = "HR Manager" });

            //Departments
            context.Departments.AddOrUpdate(d => d.DepartmentId, new Department() { DepartmentId = 1, DepartmentName = "Sales" });
            context.Departments.AddOrUpdate(d => d.DepartmentId, new Department() { DepartmentId = 2, DepartmentName = "HR Department" });
            context.Departments.AddOrUpdate(d => d.DepartmentId, new Department() { DepartmentId = 3, DepartmentName = "Admin Office" });


            // Lead Module Details

            for (var i = 1; i <= 5; i++)
            {
                var ii = i.ToString();
                // Lead Status
                context.LeadStatuses.AddOrUpdate(new LeadStatus()
                {
                    Code = "00" + ii,
                    Name = "Status 00" + ii,
                    IsDeleted = false,
                    CreatedBy = "12345ID",
                    CreatedAt = DateTime.Now
                });

                // Industry
                context.Industries.AddOrUpdate(new Industry()
                {
                    Code = "00" + ii,
                    Name = "Industry 00" + ii,
                    IsDeleted = false,
                    CreatedBy = "12345ID",
                    CreatedAt = DateTime.Now
                });

                // Lead Source
                context.LeadSources.AddOrUpdate(new LeadSource()
                {
                    Code = "00" + ii,
                    Name = "Source 00" + ii,
                    IsDeleted = false,
                    CreatedBy = "12345ID",
                    CreatedAt = DateTime.Now
                });

                // Seed AspNetUsers Table
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var PasswordHash = new PasswordHasher();
                var user = new ApplicationUser
                {
                    UserName = "johnpaulgreganda@gmail.com",
                    Email = "johnpaulgreganda@gmail.com",
                    PasswordHash = PasswordHash.HashPassword("12345"),
                    UserAccount = new UserAccount
                    {
                        EmployeeId = 4,
                        Username = "abcd@gmail.com",
                        Password = PasswordHash.HashPassword("12345"),
                        IsDeactivated = false,
                        CreatedBy = "12345ID",
                        CreatedAt = DateTime.Now
                    }
                };
                manager.Create(user);
            }
        }
    }
}
