namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterModelBase : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employee", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.Employee", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Industry", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.Industry", "UpdatedBy", c => c.String());
            AlterColumn("dbo.Lead", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.Lead", "UpdatedBy", c => c.String());
            AlterColumn("dbo.LeadSource", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.LeadSource", "UpdatedBy", c => c.String());
            AlterColumn("dbo.LeadStatus", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.LeadStatus", "UpdatedBy", c => c.String());
            AlterColumn("dbo.UserAccount", "CreatedBy", c => c.String(nullable: false));
            AlterColumn("dbo.UserAccount", "UpdatedBy", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserAccount", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.UserAccount", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.LeadStatus", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.LeadStatus", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.LeadSource", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.LeadSource", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Lead", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.Lead", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Industry", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.Industry", "CreatedBy", c => c.Int(nullable: false));
            AlterColumn("dbo.Employee", "UpdatedBy", c => c.Int());
            AlterColumn("dbo.Employee", "CreatedBy", c => c.Int(nullable: false));
        }
    }
}
