namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterEmployeeModel : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Employee", name: "Department_DepartmentId", newName: "DepartmentId");
            RenameColumn(table: "dbo.Employee", name: "Position_PositionId", newName: "PositionId");
            RenameIndex(table: "dbo.Employee", name: "IX_Position_PositionId", newName: "IX_PositionId");
            RenameIndex(table: "dbo.Employee", name: "IX_Department_DepartmentId", newName: "IX_DepartmentId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Employee", name: "IX_DepartmentId", newName: "IX_Department_DepartmentId");
            RenameIndex(table: "dbo.Employee", name: "IX_PositionId", newName: "IX_Position_PositionId");
            RenameColumn(table: "dbo.Employee", name: "PositionId", newName: "Position_PositionId");
            RenameColumn(table: "dbo.Employee", name: "DepartmentId", newName: "Department_DepartmentId");
        }
    }
}
