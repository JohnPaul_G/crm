namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserAccountIdFieldToAspNetUsersTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Employee", new[] { "EmployeeId" });
            AddColumn("dbo.AspNetUsers", "UserAccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "UserAccountId");
            AddForeignKey("dbo.AspNetUsers", "UserAccountId", "dbo.UserAccount", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "UserAccountId", "dbo.UserAccount");
            DropIndex("dbo.AspNetUsers", new[] { "UserAccountId" });
            DropColumn("dbo.AspNetUsers", "UserAccountId");
            CreateIndex("dbo.Employee", "EmployeeId", unique: true);
        }
    }
}
