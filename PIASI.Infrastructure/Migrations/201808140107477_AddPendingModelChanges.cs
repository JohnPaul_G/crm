namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPendingModelChanges : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Employee", new[] { "EmployeeId" });
            AlterColumn("dbo.LeadPhone", "PhoneNo", c => c.String(nullable: false, maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LeadPhone", "PhoneNo", c => c.String(nullable: false));
            CreateIndex("dbo.Employee", "EmployeeId", unique: true);
        }
    }
}
