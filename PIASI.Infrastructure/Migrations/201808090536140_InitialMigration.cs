namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreateUserRequest",
                c => new
                    {
                        UniqueId = c.String(nullable: false, maxLength: 128),
                        EmployeeId = c.Int(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        RequestType = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UniqueId)
                .ForeignKey("dbo.Employee", t => t.EmployeeId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        LastName = c.String(nullable: false, maxLength: 150),
                        FirstName = c.String(nullable: false, maxLength: 150),
                        MiddleName = c.String(nullable: false, maxLength: 150),
                        Email = c.String(nullable: false, maxLength: 100),
                        ContactNumber = c.String(nullable: false, maxLength: 100),
                        Birthdate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                        Department_DepartmentId = c.Int(nullable: false),
                        Position_PositionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.Department_DepartmentId)
                .ForeignKey("dbo.Position", t => t.Position_PositionId)
                .Index(t => t.EmployeeId, unique: true)
                .Index(t => t.Department_DepartmentId)
                .Index(t => t.Position_PositionId);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Position",
                c => new
                    {
                        PositionId = c.Int(nullable: false, identity: true),
                        PositionName = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.PositionId);
            
            CreateTable(
                "dbo.Industry",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LeadAssignedUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LeadId = c.Int(nullable: false),
                        UserAccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lead", t => t.LeadId)
                .ForeignKey("dbo.UserAccount", t => t.UserAccountId)
                .Index(t => t.LeadId)
                .Index(t => t.UserAccountId);
            
            CreateTable(
                "dbo.Lead",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(nullable: false, maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        MiddleName = c.String(nullable: false, maxLength: 100),
                        Title = c.String(maxLength: 300),
                        Company = c.String(maxLength: 200),
                        Website = c.String(maxLength: 100),
                        Street = c.String(maxLength: 100),
                        Province = c.String(maxLength: 100),
                        City = c.String(maxLength: 100),
                        PostalCode = c.String(maxLength: 20),
                        Country = c.String(maxLength: 100),
                        LeadStatusId = c.Int(),
                        LeadSourceId = c.Int(),
                        IndustryId = c.Int(),
                        DoNotCall = c.Boolean(nullable: false),
                        CallAvailability = c.String(maxLength: 120),
                        Description = c.String(maxLength: 300),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Industry", t => t.IndustryId)
                .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
                .ForeignKey("dbo.LeadStatus", t => t.LeadStatusId)
                .Index(t => t.LeadStatusId)
                .Index(t => t.LeadSourceId)
                .Index(t => t.IndustryId);
            
            CreateTable(
                "dbo.LeadEmailAddress",
                c => new
                    {
                        LeadEmailAddressId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        LeadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LeadEmailAddressId)
                .ForeignKey("dbo.Lead", t => t.LeadId)
                .Index(t => t.LeadId);
            
            CreateTable(
                "dbo.LeadPhone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LeadId = c.Int(nullable: false),
                        PhoneNo = c.String(nullable: false),
                        IsPrimary = c.Boolean(nullable: false),
                        Type = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lead", t => t.LeadId)
                .Index(t => t.LeadId);
            
            CreateTable(
                "dbo.LeadSource",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LeadStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserAccount",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        Username = c.String(nullable: false, maxLength: 150),
                        Password = c.String(nullable: false, maxLength: 150),
                        IsDeactivated = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedBy = c.Int(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LeadAssignedUser", "UserAccountId", "dbo.UserAccount");
            DropForeignKey("dbo.UserAccount", "EmployeeId", "dbo.Employee");
            DropForeignKey("dbo.LeadAssignedUser", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.Lead", "LeadStatusId", "dbo.LeadStatus");
            DropForeignKey("dbo.Lead", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.LeadPhone", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.Lead", "IndustryId", "dbo.Industry");
            DropForeignKey("dbo.LeadEmailAddress", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.CreateUserRequest", "EmployeeId", "dbo.Employee");
            DropForeignKey("dbo.Employee", "Position_PositionId", "dbo.Position");
            DropForeignKey("dbo.Employee", "Department_DepartmentId", "dbo.Department");
            DropIndex("dbo.UserAccount", new[] { "EmployeeId" });
            DropIndex("dbo.LeadPhone", new[] { "LeadId" });
            DropIndex("dbo.LeadEmailAddress", new[] { "LeadId" });
            DropIndex("dbo.Lead", new[] { "IndustryId" });
            DropIndex("dbo.Lead", new[] { "LeadSourceId" });
            DropIndex("dbo.Lead", new[] { "LeadStatusId" });
            DropIndex("dbo.LeadAssignedUser", new[] { "UserAccountId" });
            DropIndex("dbo.LeadAssignedUser", new[] { "LeadId" });
            DropIndex("dbo.Employee", new[] { "Position_PositionId" });
            DropIndex("dbo.Employee", new[] { "Department_DepartmentId" });
            DropIndex("dbo.Employee", new[] { "EmployeeId" });
            DropIndex("dbo.CreateUserRequest", new[] { "EmployeeId" });
            DropTable("dbo.UserAccount");
            DropTable("dbo.LeadStatus");
            DropTable("dbo.LeadSource");
            DropTable("dbo.LeadPhone");
            DropTable("dbo.LeadEmailAddress");
            DropTable("dbo.Lead");
            DropTable("dbo.LeadAssignedUser");
            DropTable("dbo.Industry");
            DropTable("dbo.Position");
            DropTable("dbo.Department");
            DropTable("dbo.Employee");
            DropTable("dbo.CreateUserRequest");
        }
    }
}
