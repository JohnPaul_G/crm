namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLeadEmailAddressStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LeadEmailAddress", "Name", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LeadEmailAddress", "Name", c => c.String(nullable: false, maxLength: 150));
        }
    }
}
