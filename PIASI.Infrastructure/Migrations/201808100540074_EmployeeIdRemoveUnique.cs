namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeeIdRemoveUnique : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Employee", new[] { "EmployeeId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Employee", "EmployeeId", unique: true);
        }
    }
}
