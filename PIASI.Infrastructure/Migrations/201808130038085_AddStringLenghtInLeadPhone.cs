namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStringLenghtInLeadPhone : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LeadPhone", "PhoneNo", c => c.String(nullable: false, maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LeadPhone", "PhoneNo", c => c.String(nullable: false));
        }
    }
}
