namespace PIASI.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteIsDeactivatedFieldToUsersTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "IsDeactivated");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "IsDeactivated", c => c.Boolean());
        }
    }
}
