﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Infrastructure
{
    public class PiasiContextInitializer : IDatabaseInitializer<PiasiContext>
    {
        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="System.ArgumentNullException">context</exception>
        /// <exception cref="System.InvalidOperationException">
        /// Database does not exist.
        /// or
        /// Model does not match the database.
        /// </exception>
        public void InitializeDatabase(PiasiContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (!context.Database.Exists())
            {
                throw new InvalidOperationException("Database does not exist.");
            }

            //if (!context.Database.CompatibleWithModel(true))
            //{
            //    throw new InvalidOperationException("Model does not match the database.");
            //}
        }

        //public static class DatabaseHelper
        //{
        //    /// <summary>
        //    /// Initializes this instance.
        //    /// </summary>
        //    public static void Initialize()
        //    {
        //        Database.SetInitializer(new PiasiContextInitializer());
        //        var context = new PiasiContext();
        //        if (!context.Database.CompatibleWithModel(true))
        //        {
        //            try
        //            {
        //                Configuration configuration = new Configuration();
        //                var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
        //                migrator.Update();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new Exception(string.Format("Error encountered {0}. Please contact your system administrator!", ex.Message));
        //            }
        //        }

        //        if (!context.Database.CompatibleWithModel(true))
        //        {
        //            throw new Exception("Database not updated.  Please contact your system administrator!");
        //        }
        //    }
        //}
    }
}
