﻿using Autofac;
using PIASI.Domain.Repository;
using PIASI.Infrastructure.Repository;

namespace PIASI.Infrastructure
{
    public class InfrastructureModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterRepositories(builder);
            base.Load(builder);
        }

        private void RegisterRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<IndustryRepository>().As<IIndustryRepository>().InstancePerLifetimeScope();
            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().InstancePerLifetimeScope();
            builder.RegisterType<PositionRepository>().As<IPositionRepository>().InstancePerLifetimeScope();
            builder.RegisterType<DepartmentRepository>().As<IDepartmentRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadStatusRepository>().As<ILeadStatusRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadSourceRepository>().As<ILeadSourceRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadAssignedUserRepository>().As<ILeadAssignedUserRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadPhoneRepository>().As<ILeadPhoneRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadEmailAddressRepository>().As<ILeadEmailAddressRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LeadRepository>().As<ILeadRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
            builder.RegisterType<AccountRepository>().As<IAccountRepository>().InstancePerLifetimeScope();
        }

    }
}
