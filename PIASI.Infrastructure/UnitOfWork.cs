﻿using PIASI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Infrastructure
{
    public interface IUnitOfWork
    {
        void SaveChanges();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDataContext _dataContext;

        public UnitOfWork(PiasiContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void SaveChanges()
        {
            _dataContext.SaveChanges();
        }
    }

    public interface IDataContext
    {
        void SaveChanges();
    }
}
