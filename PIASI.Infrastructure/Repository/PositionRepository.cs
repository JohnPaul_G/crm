﻿using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace PIASI.Infrastructure.Repository
{
    public class PositionRepository:RepositoryBase,IPositionRepository
    {
        public PositionRepository(PiasiContext context)
        :base(context)
        {
        }

        public IEnumerable<Position> GetPositions()
        {
            return _context.Positions.OrderBy(p => p.PositionName).ToList();
        }
    }
}
