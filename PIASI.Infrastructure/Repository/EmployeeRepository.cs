﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace PIASI.Infrastructure.Repository
{
    public class EmployeeRepository : RepositoryBase,IEmployeeRepository 
    {
        public EmployeeRepository(PiasiContext context)
        :base(context)
        {
        }

        public Employee GetEmployeeById(int id)
        {
            return _context.Employees.Find(id);
        }
        
        public bool IdExists(int employeeid)
        {
            return _context.Employees.Any(e => e.EmployeeId == employeeid && e.IsDeleted==false);
        }

        public IEnumerable<Employee> GetEmployeeDataTableData(
        int? employeeid,
        string name,
        string email,
        string birthdate,
        string contactnumber,
        string position,
        string department,
        DataFilter filter)
        {
            var data = _context.Employees
                .Where(e=>e.IsDeleted==false)
                .Include(e => e.Position)
                .Include(e => e.Department)
                .AsQueryable();

            filter.TotalRecordCount = data.Count();

            /// Filter

            if (employeeid > 0)
            {
                data = data.Where(e => e.EmployeeId.ToString().Contains(employeeid.ToString()));
            }
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(e => e.LastName.Contains(name) || e.FirstName.Contains(name) || e.MiddleName.Contains(name));
            }
            if (!string.IsNullOrEmpty(email))
            {
                data = data.Where(e => e.Email.Contains(email));
            }
            if (!string.IsNullOrEmpty(birthdate))
            {
                string[] daterange = birthdate.Split('-');
                var date1 = DateTime.Parse(daterange[0]);
                var date2 = DateTime.Parse(daterange[1]);
                data = data.Where(e => e.Birthdate>=date1 && e.Birthdate<=date2);
            }
            if (!string.IsNullOrEmpty(contactnumber))
            {
                data = data.Where(e => e.ContactNumber.Contains(contactnumber));
            }
            if (!string.IsNullOrEmpty(position))
            {
                data = data.Where(e => e.Position.PositionName.Contains(position));
            }
            if (!string.IsNullOrEmpty(department))
            {
                data = data.Where(e => e.Department.DepartmentName.Contains(department));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            if (filter.SortColumn == "Name")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(e => e.LastName) : data.OrderByDescending(e => e.LastName);
            }
            else if (filter.SortColumn == "Position")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(e => e.Position.PositionName) : data.OrderByDescending(e => e.Position.PositionName);
            }
            else if (filter.SortColumn == "Department")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(e => e.Department.DepartmentName) : data.OrderByDescending(e => e.Department.DepartmentName);
            }
            else
            {
                data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);
            }

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }
    }
}
