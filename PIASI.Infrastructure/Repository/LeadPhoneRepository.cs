﻿using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIASI.Domain.Model;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace PIASI.Infrastructure.Repository
{
    public class LeadPhoneRepository : ILeadPhoneRepository
    {
        public readonly PiasiContext _context;

        public LeadPhoneRepository(PiasiContext context)
        {
            _context = context;
            //_context.Configuration.ProxyCreationEnabled = false;
            //_context.Configuration.LazyLoadingEnabled = true;
        }

        public List<LeadPhone> Add(List<LeadPhone> leadPhones)
        {
            try
            {
                foreach(var leadphone in leadPhones)
                {
                    _context.LeadPhones.Add(leadphone);
                }
                
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }

            return leadPhones;
        }

        public IEnumerable<LeadPhone> GetAll()
        {
            return _context.LeadPhones.ToList();
        }

        public string RemoveByLeadId(int leadId)
        {
            try
            {
                var leadPhones = _context.LeadPhones.Where(x => x.LeadId == leadId);

                foreach(var leadphone in leadPhones)
                {
                    _context.LeadPhones.Remove(leadphone);
                }

                _context.SaveChanges();
            }
            catch (Exception)
            {
                return "ERROR";
            }

            return "OK";
        }

        public Tuple<bool, string> UpdateByLeadId(List<LeadPhone> leadPhones, int leadId)
        {
            var hasError = false;
            var message = "Ok";
           
            try
            {
                _context.LeadPhones.RemoveRange(_context.LeadPhones.Where(x => x.LeadId == leadId)); // remove all first

                foreach (var leadphone in leadPhones)
                {
                    //_context.LeadPhones.AddOrUpdate(leadphone);
                    _context.LeadPhones.Add(leadphone);
                }

                _context.SaveChanges(); 
            }
            catch (Exception ex)
            {
                hasError = true;
                message = ex.ToString();
            }

            return new Tuple<bool, string>(hasError, message);
        }
    }
}
