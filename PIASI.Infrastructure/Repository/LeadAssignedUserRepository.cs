﻿using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIASI.Domain.Model;
using System.Data.Entity;

namespace PIASI.Infrastructure.Repository
{
    public class LeadAssignedUserRepository : ILeadAssignedUserRepository
    {
        private readonly PiasiContext _context;

        public LeadAssignedUserRepository(PiasiContext context)
        {
            _context = context;
        //    _context.Configuration.ProxyCreationEnabled = false;
        //    _context.Configuration.LazyLoadingEnabled = true;
        }

        public IEnumerable<LeadAssignedUser> GetAllAssignedUserByLeadId(int leadId)
        {
            return _context.LeadAssignedUsers
                .Include(x => x.UserAccount).Where(x => x.LeadId == leadId)
                .Include(x => x.UserAccount.Employee)
                .ToList();
        }

        public List<LeadAssignedUser> Add(List<LeadAssignedUser> leadAssignedUsers)
        {
            try
            {
                foreach (var leadAssignedUser in leadAssignedUsers)
                {
                    _context.LeadAssignedUsers.Add(leadAssignedUser);
                }
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }

            return leadAssignedUsers;
        }

        public Tuple<bool, string> UpdateByLeadId(List<LeadAssignedUser> leadAssignedUsers, int leadId)
        {
            var hasError = false;
            var message = "Ok";

            try
            {
                _context.LeadAssignedUsers.RemoveRange(_context.LeadAssignedUsers.Where(x => x.LeadId == leadId)); // remove all first

                foreach (var leadAssignedUser in leadAssignedUsers)
                {
                    _context.LeadAssignedUsers.Add(leadAssignedUser);
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                hasError = true;
                message = ex.ToString();
            }

            return new Tuple<bool, string>(hasError, message);
        }
    }
}
