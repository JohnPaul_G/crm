﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Infrastructure.Repository
{
    public class LeadStatusRepository : RepositoryBase, ILeadStatusRepository
    {
        public LeadStatusRepository(PiasiContext context) 
            : base(context)
        { 
        }

        public bool CodeExists(string code, int id = 0)
        {
            return _context.LeadStatuses.Any(x => x.Code.ToUpper() == code.ToUpper() && x.Id != id && x.IsDeleted == false);
        }

        public IEnumerable<LeadStatus> GetAll()
        {
            return _context.LeadStatuses.Where(x => x.IsDeleted == false);
        }

        public LeadStatus GetLeadStatusById(int id)
        {
            return _context.LeadStatuses.Find(id);
        }

        public IEnumerable<LeadStatus> GetGridViewData(string code, string name, DataFilter filter)
        {
            var data = _context.LeadStatuses.Where(x => x.IsDeleted == false).AsQueryable();

            filter.TotalRecordCount = data.Count();

            //// Filter
            data = data.Where(p => p.IsDeleted == false);

            if (!string.IsNullOrEmpty(code))
            {
                data = data.Where(p => p.Code.Contains(code));
            }
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(p => p.Name.Contains(name));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        //public string Remove(int id)
        //{
        //    try
        //    {
        //        var leadStatus = _context.LeadStatuses.Find(id);
        //        leadStatus.UpdatedBy = 1; //<---temp
        //        leadStatus.UpdatedAt = DateTime.Now; //<---temp
        //        leadStatus.IsDeleted = true;
        //        // _context.Entry(classification).State = EntityState.Deleted;

        //        _context.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        return "ERROR";
        //    }

        //    return "OK";
        //}
    }
}
