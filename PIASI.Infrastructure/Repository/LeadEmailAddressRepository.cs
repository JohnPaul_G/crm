﻿using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIASI.Domain.Model;

namespace PIASI.Infrastructure.Repository
{
    public class LeadEmailAddressRepository : ILeadEmailAddressRepository
    {
        private readonly PiasiContext _context;

        public LeadEmailAddressRepository(PiasiContext context)
        {
            _context = context;
            //_context.Configuration.ProxyCreationEnabled = false;
            //_context.Configuration.LazyLoadingEnabled = true;
        }

        public List<LeadEmailAddress> Add(List<LeadEmailAddress> leadEmailAddresses)
        {
            try
            {
                foreach (var leadEmailAddress in leadEmailAddresses)
                {
                    _context.LeadEmailAddresses.Add(leadEmailAddress);
                }
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }

            return leadEmailAddresses;
        }

        public IEnumerable<LeadEmailAddress> GetAll(int leadId)
        {
            return _context.LeadEmailAddresses.ToList();
        }

        public Tuple<bool, string> UpdateByLeadId(List<LeadEmailAddress> leadEmailAddresses, int leadId)
        {
            var hasError = false;
            var message = "Ok";

            try
            {
                _context.LeadEmailAddresses.RemoveRange(_context.LeadEmailAddresses.Where(x => x.LeadId == leadId)); // remove all first

                foreach (var leadEmailAddress in leadEmailAddresses)
                {
                    _context.LeadEmailAddresses.Add(leadEmailAddress);
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                hasError = true;
                message = ex.ToString();
            }

            return new Tuple<bool, string>(hasError, message);
        }
    }
}
