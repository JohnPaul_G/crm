﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;

namespace PIASI.Infrastructure.Repository
{
    public class LeadRepository : RepositoryBase, ILeadRepository
    {

        public LeadRepository(PiasiContext context)
            : base(context)
        {
        }

        public Lead Add(Lead lead)
        {
            //if (_context.Industries.Any(x => x.Code.ToUpper() == industry.Code.ToUpper()))  // Do not allow if Code will Duplicate
            //{
            //    return null;
            //}

            try
            {
                _context.Leads.Add(lead);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }

            return lead;
        }

        public bool CodeExists(int id = 0)
        {
            return _context.Leads.Any(x => x.Id != id && x.IsDeleted == false);
        }

        public IEnumerable<Lead> GetAll()
        {
            return _context.Leads.Where(x => x.IsDeleted == false);
        }

        public Lead GetLeadById(int id)
        {
            // _context.Leads.Find(id); IBABALIK KO TO KAPAG NO NEED TO NA YUNG .Include
            //return _context.Leads
            //    .Where(x => x.Id == id)
            //    .Include(x => x.LeadPhones)
            //    .Include(x => x.EmailAddresses)
            //    .Include(x => x.AssignedUsers)
            //    .FirstOrDefault();

            return _context.Leads.Find(id);
        }

        public IEnumerable<Lead> GetGridViewData(string name, string company, string email, string website, string assignedUser, string createdAt, DataFilter filter)
        {
            //var userAccount = _context.UserAccounts.ToList();
            var data = _context.Leads
                //.Include(x => x.EmailAddresses)
                //.Include(x => x.AssignedUsers)
                //.Include(x => x.LeadPhones)
                .Where(x => x.IsDeleted == false)
                .AsQueryable();

            filter.TotalRecordCount = data.Count();

            //// Filter
            data = data.Where(p => p.IsDeleted == false);


            if (!string.IsNullOrEmpty(name))
            {
                name = name.Replace(",", "");
                if (name == " ")
                {
                    data = data.Where(p => (p.LastName + p.FirstName + p.MiddleName).Contains(name));
                }
                else
                {
                    data = data.Where(p => (p.LastName + " " + p.FirstName + " " + p.MiddleName).Contains(name));
                }
            }
            if (!string.IsNullOrEmpty(company))
            {
                data = data.Where(p => p.Company.Contains(company));
            }
            if (!string.IsNullOrEmpty(assignedUser))
            {
                //data = data.Where(x => x.AssignedUsers.FirstOrDefault().UserAccount.Username.Contains(assignedUser));
                assignedUser = assignedUser.Replace(",", "");
                data = data.Where(x => (x.AssignedUsers.FirstOrDefault().UserAccount.Employee.LastName + " " +
                                        x.AssignedUsers.FirstOrDefault().UserAccount.Employee.FirstName + " " +
                                        x.AssignedUsers.FirstOrDefault().UserAccount.Employee.MiddleName)
                                        .Contains(assignedUser));
            }
            if (!string.IsNullOrEmpty(email))
            {
                data = data.Where(x => x.EmailAddresses.FirstOrDefault().Name.Contains(email));
            }
            if (!string.IsNullOrEmpty(website))
            {
                data = data.Where(p => p.Website.Contains(website));
            }
            if (!string.IsNullOrEmpty(createdAt))
            {
                var dates = createdAt.Split('-');
                var dateFrom = DateTime.Parse(dates[0]);
                var dateTo = DateTime.Parse(dates[1]);

                data = data.Where(p => p.CreatedAt >= dateFrom && p.CreatedAt <= dateTo);
            }
            filter.FilteredRecordCount = data.Count();

            // Sort
            data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        public string Remove(int id)
        {
            try
            {
                var lead = _context.Leads.Find(id);
                //lead.UpdatedBy = User.Identity.GetUserId(),
                //lead.UpdatedAt = DateTime.Now; //<---temp
                lead.IsDeleted = true;
                // _context.Entry(classification).State = EntityState.Deleted;

                _context.SaveChanges();
            }
            catch (Exception)
            {
                return "ERROR";
            }

            return "OK";
        }

        public Lead Update(Lead lead)
        {
            //if (_context.Industries.Any(x => x.Code.ToUpper() == industry.Code.ToUpper() && x.IndustryId != industry.IndustryId))  // Do not allow if Code will Duplicate
            //{
            //    return null;
            //}
            var ld = _context.Leads.Find(lead.Id);
            try
            {
                ld.LastName = lead.LastName;
                ld.FirstName = lead.FirstName;
                ld.MiddleName = lead.MiddleName;
                ld.CallAvailability = lead.CallAvailability;
                ld.DoNotCall = lead.DoNotCall;
                ld.Title = lead.Title;
                ld.Company = lead.Company;
                ld.Website = lead.Website;
                ld.Street = lead.Street;
                ld.Province = lead.Province;
                ld.City = lead.City;
                ld.PostalCode = lead.PostalCode;
                ld.Country = lead.Country;
                ld.LeadStatusId = lead.LeadStatusId;
                ld.LeadSourceId = lead.LeadSourceId;
                ld.IndustryId = lead.IndustryId;
                ld.Description = lead.Description;
                ld.UpdatedBy = lead.UpdatedBy;
                ld.UpdatedAt = lead.UpdatedAt;

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return null;
            }

            return lead;
        }
    }
}
