﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Infrastructure.Repository
{
    public class LeadSourceRepository : RepositoryBase, ILeadSourceRepository
    {
        public LeadSourceRepository(PiasiContext context) : 
            base(context)
        {
        }

        public bool CodeExists(string code, int id = 0)
        {
            return _context.LeadSources.Any(x => x.Code.ToUpper() == code.ToUpper() && x.Id != id && x.IsDeleted == false);
        }

        public IEnumerable<LeadSource> GetAll()
        {
            return _context.LeadSources.Where(x => x.IsDeleted == false);
        }

        public LeadSource GetLeadSourceById(int id)
        {
            return _context.LeadSources.Find(id);
        }

        public IEnumerable<LeadSource> GetGridViewData(string code, string name, DataFilter filter)
        {
            var data = _context.LeadSources.Where(x => x.IsDeleted == false).AsQueryable();

            filter.TotalRecordCount = data.Count();

            //// Filter
            data = data.Where(p => p.IsDeleted == false);

            if (!string.IsNullOrEmpty(code))
            {
                data = data.Where(p => p.Code.Contains(code));
            }
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(p => p.Name.Contains(name));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        //public string Remove(int id)
        //{
        //    try
        //    {
        //        var leadSource = _context.LeadSources.Find(id);
        //        leadSource.UpdatedBy = 1; //<---temp
        //        leadSource.UpdatedAt = DateTime.Now; //<---temp
        //        leadSource.IsDeleted = true;
        //        // _context.Entry(classification).State = EntityState.Deleted;

        //        _context.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        return "ERROR";
        //    }

        //    return "OK";
        //}
    }
}
