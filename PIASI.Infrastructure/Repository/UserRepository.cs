﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace PIASI.Infrastructure.Repository
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(PiasiContext context)
            :base(context)
        {
            
        }

        public IEnumerable<UserAccount> EmpployeeWithAccountsGridViewData(
            int? empId,
            string name,
            string position,
            string department,
            Boolean? isactive,
            DataFilter filter
            )
        {
//            var data = _context.Employees
//                .Where(e => _context.UserAccounts.Select(u => u.EmployeeId).Contains(e.EmployeeId))
//                .Include(e => e.Position)
//                .Include(e => e.Department)
//                .AsQueryable();

            var data = _context.UserAccounts.Where(u =>
                    _context.Employees.Select(e => e.Id).Contains(u.EmployeeId) && !u.IsDeactivated && !u.Employee.IsDeleted)
                .Include(u => u.Employee.Position)
                .Include(u => u.Employee.Department)
                .AsQueryable();
            
            filter.TotalRecordCount = data.Count();
            
            /// Filter
            if (empId > 0)
            {
                data = data.Where(e => e.Employee.EmployeeId == empId);
            }

            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(e => e.Employee.LastName.Contains(name) || e.Employee.FirstName.Contains(name) || e.Employee.MiddleName.Contains(name));
            }

            if (!string.IsNullOrEmpty(position))
            {
                data = data.Where(e => e.Employee.Position.PositionName.Contains(position));
            }
            if (!string.IsNullOrEmpty(department))
            {
                data = data.Where(e => e.Employee.Department.DepartmentName.Contains(department));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            if (filter.SortColumn == "Name")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.Employee.LastName) : data.OrderByDescending(p => p.Employee.LastName);
            }
            else if (filter.SortColumn == "Position")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.Employee.Position.PositionName) : data.OrderByDescending(p => p.Employee.Position.PositionName);
            }
            else if ( filter.SortColumn == "Department")
            {
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.Employee.Department.DepartmentName) : data.OrderByDescending(p => p.Employee.Department.DepartmentName);
            }
            else
            {
                data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);
            }
            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        public IEnumerable<Employee> EmpployeeWithoutAccountsGridViewData(
            int? empId,
            string name,
            string position,
            string department,
            DataFilter filter
        )
        {
            var data = _context.Employees
                .Where(e => !_context.UserAccounts.Select(u => u.EmployeeId).Contains(e.Id) && e.IsActive && !e.IsDeleted)
                .Include(e => e.Position)
                .Include(e => e.Department)
                .AsQueryable();

            filter.TotalRecordCount = data.Count();

            /// Filter

            if (empId > 0)
            {
                data = data.Where(e => e.EmployeeId == empId);
            }
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(e => e.LastName.Contains(name) || e.FirstName.Contains(name) || e.MiddleName.Contains(name));
            }

            if (!string.IsNullOrEmpty(position))
            {
                data = data.Where(e => e.Position.PositionName.Contains(position));
            }
            if (!string.IsNullOrEmpty(department))
            {
                data = data.Where(e => e.Department.DepartmentName.Contains(department));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            if (filter.SortColumn == "Name")
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.LastName) : data.OrderByDescending(p => p.LastName);

            else if (filter.SortColumn == "Position")
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.Position.PositionName) : data.OrderByDescending(p => p.Position.PositionName);

            else if (filter.SortColumn == "Department")
                data = filter.SortDirection == "asc" ? data.OrderBy(p => p.Department.DepartmentName) : data.OrderByDescending(p => p.Department.DepartmentName);

            else
                data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        public bool SaveAddUserRequest(int empId, string requestType)
        {
            try
            {
                var token = Guid.NewGuid().ToString("N");
                var employee = _context.Employees.Single(e => e.EmployeeId == empId);
                var request = new CreateUserRequest
                {
                    UniqueId = token,
                    Employee = employee,
                    ExpirationDate = DateTime.Now.AddDays(1),
                    RequestType = requestType
                };
                SendEmailToCreateUser(employee, token, requestType);

                _context.CreateUserRequests.Add(request);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public void SendEmailToCreateUser(Employee employee, string uniqueId, string requestType)
        {
            var sender = new MailAddress("crmt11983@gmail.com", "Gourmet Farms, Inc.");
            var recepient = new MailAddress(employee.Email);
            var mailMessage = new MailMessage(sender, recepient);
            var emailBody = new StringBuilder();

            // Embed company logo (Using WebCLient - by means of image url)
            //string imageWebUrlPath = "http://localhost:60519/Content/img/gourmet.png";
            //var webClient = new WebClient();
            //byte[] imageBytes = webClient.DownloadData(imageWebUrlPath);
            //MemoryStream ms = new MemoryStream(imageBytes);

            // Embed company logo using Embedded Resource
            var bmp = new Bitmap(PIASI.Infrastructure.Properties.Resources.gourmet);
            ImageConverter ic = new ImageConverter();
            byte[] ba = (byte[]) ic.ConvertTo(bmp, typeof(byte[]));
            MemoryStream ms = new MemoryStream(ba);

            var finalLogo = new Attachment(ms, MediaTypeNames.Image.Jpeg);
            mailMessage.Attachments.Add(finalLogo);
            finalLogo.ContentId = "CompanyLogo";
            finalLogo.ContentDisposition.Inline = true;
            finalLogo.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

            emailBody.Append("<img src='cid:CompanyLogo' width='350' height='120' /><br>");

            emailBody.Append("Dear " + employee.FirstName + ", <br><br>");

            if (requestType == "Register")
            {
                emailBody.Append("Please click the link below to create your account. <br>");
                emailBody.Append("<a href='http://localhost:60519/User/Register?uid=" + uniqueId + "'>localhost:60519/User/Register?uid=" + uniqueId + "</a>");
                mailMessage.Subject = "Create your CRM account";
            }
            if (requestType == "ResetPassword")
            {
                emailBody.Append("Please click the link below to reset your Password. <br>");
                emailBody.Append("<a href='http://localhost:60519/User/Reset?uid=" + uniqueId + "'>localhost:60519/User/Reset?uid=" + uniqueId + "</a>");
                mailMessage.Subject = "Reset your CRM Account Password";
            }
            emailBody.Append("<br><br>");
            emailBody.Append("<i><b>Note:</b> The link above will expire after 24 hours.</i><br><br>");

            mailMessage.IsBodyHtml = true;
            mailMessage.Body = emailBody.ToString();
            mailMessage.BodyEncoding = Encoding.UTF8;
            
            var smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "crmt11983@gmail.com",
                Password = "crmtest123"
            };

            smtpClient.EnableSsl = true;
            smtpClient.Timeout = 500000;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Send(mailMessage);
        }
        public CreateUserRequest GetUserRequestByUID(string uid)
        {
            //return _context.CreateUserRequests.SingleOrDefault(u => u.UniqueId.Equals(uid));
            return _context.CreateUserRequests.Where(u => u.UniqueId == uid)
                .Include(u => u.Employee)
                .SingleOrDefault();
        }
        public bool SaveUserAccount(UserAccount userAccount, string uid)
        {
            try
            {
                _context.UserAccounts.Add(userAccount);
                this.DeleteUserAccountRequest(uid);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void DeleteUserAccountRequest(string uid)
        {
            var request = _context.CreateUserRequests.SingleOrDefault(u => u.UniqueId == uid);
            _context.CreateUserRequests.Remove(request);
            _context.SaveChanges();
        }
        public IEnumerable<UserAccount> GetAll()
        {
            return _context.UserAccounts
                            .Include(x => x.Employee)
                            .Include(x => x.Employee.Department)
                            .ToList();
        }
        public void DeactivateUserAccount(int empId)
        {
            var userAccount = _context.UserAccounts.Where(u => u.EmployeeId == empId).Include(u => u.Employee).FirstOrDefault();
            userAccount.IsDeactivated = true;

            _context.SaveChanges();
        }
        public UserAccount GetUserAccount(int empId)
        {
            return _context.UserAccounts.Single(u => u.EmployeeId == empId);
        }
        public string PasswordHash(string password)
        {
            byte [] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }
    }
}
