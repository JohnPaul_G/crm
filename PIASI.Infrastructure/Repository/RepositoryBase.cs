﻿using System;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System.Data.Entity;

namespace PIASI.Infrastructure.Repository
{
    public class RepositoryBase:IRepositoryBase
    {
        private readonly PiasiContext context;

        public RepositoryBase(PiasiContext context)
        {
            this.context = context;
            //this.context.Configuration.ProxyCreationEnabled = false;
            //this.context.Configuration.LazyLoadingEnabled = true;
        }

        public PiasiContext _context
        {
            get { return this.context; }
        }

        public void InsertOrUpdate(ModelBase entity)
        {
            if (entity.Id == 0)
            {
                _context.Entry(entity).State = EntityState.Added;
            }
            else
            {
                _context.Entry(entity).State = EntityState.Modified;
            }

            SaveChanges();
        }

        public int SaveChanges()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return 0;
        }

    }
}
