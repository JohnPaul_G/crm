﻿using PIASI.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Infrastructure.Repository
{
    public class AccountRepository : RepositoryBase, IAccountRepository
    {
        public AccountRepository(PiasiContext context)
            :base (context)
        {

        }

    }
}
