﻿using PIASI.Common.Entities;
using PIASI.Common.Helpers;
using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace PIASI.Infrastructure.Repository
{
    public class IndustryRepository : RepositoryBase, IIndustryRepository
    {
        public IndustryRepository(PiasiContext context) 
            : base(context)
        {
        }

        public bool CodeExists(string code, int id = 0)
        {
            return _context.Industries.Any(x => x.Code.ToUpper() == code.ToUpper() && x.Id != id && x.IsDeleted == false);
        }

        public IEnumerable<Industry> GetAll()
        {
            return _context.Industries.Where(x => x.IsDeleted == false);
        }

        public Industry GetIndustryById(int id)
        {
            return _context.Industries.Find(id);
        }

        public IEnumerable<Industry> GetGridViewData(string code, string name, DataFilter filter)
        {
            var data = _context.Industries.Where(x => x.IsDeleted == false).AsQueryable();

            filter.TotalRecordCount = data.Count();

            //// Filter
            data = data.Where(p => p.IsDeleted == false);

            if (!string.IsNullOrEmpty(code))
            {
                data = data.Where(p => p.Code.Contains(code));
            }
            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(p => p.Name.Contains(name));
            }

            filter.FilteredRecordCount = data.Count();

            // Sort
            data = QueryHelper.Ordering(data, filter.SortColumn, filter.SortDirection != "asc", false);

            // Paging
            if (filter.Length > 0)
            {
                data = data.Skip(filter.Start).Take(filter.Length);
            }

            return data;
        }

        //public string Remove(int id)
        //{
        //    try
        //    {
        //        var industry = _context.Industries.Find(id);
        //        industry.UpdatedBy = 1; //<---temp
        //        industry.UpdatedAt = DateTime.Now; //<---temp
        //        industry.IsDeleted = true;
        //        // _context.Entry(classification).State = EntityState.Deleted;

        //        _context.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        return "ERROR";
        //    }

        //    return "OK";
        //}
    }
}

