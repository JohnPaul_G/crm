﻿using PIASI.Domain.Model;
using PIASI.Domain.Repository;
using System.Collections.Generic;
using System.Linq;

namespace PIASI.Infrastructure.Repository
{
    public class DepartmentRepository : RepositoryBase, IDepartmentRepository
    {
        public DepartmentRepository(PiasiContext context)
        :base(context)
        {
        }

        public IEnumerable<Department> GetDepartments()
        {
            return _context.Departments.OrderBy(d=>d.DepartmentName).ToList();
        }
    }
}
