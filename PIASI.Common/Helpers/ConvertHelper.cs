﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;

namespace PIASI.Common.Helpers
{
    /// <summary>
    /// The convert helper class.
    /// </summary>
    public class ConvertHelper
    {
        /// <summary>
        /// To the integer.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The integer value.</returns>
        public static int ToInteger(object value)
        {
            if (value == null)
            {
                return 0;
            }

            var val = value.ToString();
            int result = 0;
            int.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            return result;
        }

        /// <summary>
        /// To the decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The decimal value</returns>
        public static decimal ToDecimal(object value)
        {
            var val = value.ToString();
            decimal result = 0;
            decimal.TryParse(val, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            return result;
        }

        /// <summary>
        /// To the date.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The date time value.</returns>
        public static DateTime? ToDate(object value)
        {
            if (value == null)
            {
                return null;
            }

            var val = value.ToString();
            DateTime result;
            if (!DateTime.TryParse(val, out result))
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// To the data table.
        /// </summary>
        /// <typeparam name="T">Model type.</typeparam>
        /// <param name="data">The data.</param>
        /// <returns>The data in datable format.</returns>
        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;
        }
    }
}