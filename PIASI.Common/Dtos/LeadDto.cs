﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Common.Dtos
{
    public class LeadDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Company { get; set; }

        public string Email { get; set; }

        public string AssignedUser { get; set; }

        public string Website { get; set; }

        public string CreatedAt { get; set; }
    }
}
