﻿using System;

namespace PIASI.Common.Dtos
{
    public class UserDto
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public Boolean IsActive{ get; set; }
    }
}
