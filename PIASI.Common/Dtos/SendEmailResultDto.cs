﻿namespace PIASI.Common.Dtos
{
    public class SendEmailResultDto
    {
        public int EmpId { get; set; }
        public bool Result { get; set; }
    }
}
