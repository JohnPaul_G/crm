﻿namespace PIASI.Common.Dtos
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Birthdate { get; set; }
        public string ContactNumber { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
    }
}
