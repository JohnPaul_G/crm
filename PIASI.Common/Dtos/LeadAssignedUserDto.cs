﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIASI.Common.Dtos
{
    public class LeadAssignedUserDto
    {
        public int Id { get; set; }

        public int LeadId { get; set; }

        public int UserAccountId { get; set; }

        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string DepartmentName { get; set; }
    }
}
